package fpo.thanh.coffeeshop.domain.repository;

import fpo.thanh.coffeeshop.shareModel.LoginModel;

public interface LoginRepository {
    LoginModel login(String usernamespace,String password);
}
