package fpo.thanh.coffeeshop.presentation.ui.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.presentation.ui.adapter.ListThucDonOnTableAdapter;

import static java.lang.Integer.max;

public class EditThucDonDialog extends DialogFragment implements View.OnClickListener {
    private Context context;
    private ListThucDonOnTableAdapter adapter;
    private int position;
    private final String tenThucDon;
    private int soLuong;
    private Double giaSanPham;
    private int checkCode=1;
    @BindView(R.id.tv_tenSanPhamDialog)
    TextView tv_tenSanPhamDialog;
    @BindView(R.id.ibtn_giamSanPhamDialog)
    ImageButton ibtn_giamGiaSanPhamDialog;
    @BindView(R.id.ibtn_tangSanPhamDialog)
    ImageButton ibtn_tangSanPhamDialog;
    @BindView(R.id.tv_soLuongSanPhamDialog)
    TextView tv_soLuongSanPhamDialog;
    @BindView(R.id.btn_capNhatSanPham)
    Button btn_capNhatSanPham;
    @BindView(R.id.ibtn_closeDialog)
    ImageButton ibtn_closeDialog;
    @BindView(R.id.tv_giaSanPhamDialog)
    TextView tv_giaSanPhamDialog;
    public EditThucDonDialog(Context context, ListThucDonOnTableAdapter adapter,int position,String tenThucDon, int soLuong,Double giaSanPham){

        this.context = context;
        this.adapter = adapter;
        this.position = position;
        this.tenThucDon = tenThucDon;
        this.soLuong = soLuong;
        this.giaSanPham = giaSanPham;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_edit_thucdon_ontable,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        ibtn_closeDialog.setOnClickListener(this);
        ibtn_giamGiaSanPhamDialog.setOnClickListener(this);
        ibtn_tangSanPhamDialog.setOnClickListener(this);
        btn_capNhatSanPham.setOnClickListener(this);
        tv_tenSanPhamDialog.setText(String.valueOf(tenThucDon));
        tv_soLuongSanPhamDialog.setText(String.valueOf(soLuong));
        tv_giaSanPhamDialog.setText(String.valueOf(giaSanPham)+"$");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_capNhatSanPham:
                if(checkCode==0){
                    AlertDialog.Builder builder=new AlertDialog.Builder(context,R.style.AlertDialogStyle);
                    builder.setMessage("Xác nhận xóa "+tenThucDon+" ra khỏi đơn hàng")
                            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(context, "Đã xóa "+tenThucDon, Toast.LENGTH_SHORT).show();
                                    EditThucDonDialog.this.dismiss();
                                    adapter.capNhatSoLuong(position,Integer.parseInt(tv_soLuongSanPhamDialog.getText().toString()));
                                }
                            }).create();
                    builder.show();
                }
                else
                    adapter.capNhatSoLuong(position,Integer.parseInt(tv_soLuongSanPhamDialog.getText().toString()));

                this.dismiss();
                break;
            case R.id.ibtn_giamSanPhamDialog:
                updateSoLuong(giamSoLuong());
                changeBackgroundButton(soLuong);
                break;
            case R.id.ibtn_tangSanPhamDialog:
                updateSoLuong(tangSoLuong());
                changeBackgroundButton(soLuong);
                break;
            case R.id.tv_soLuongSanPhamDialog:
                break;
            case R.id.tv_tenSanPhamDialog:
                break;
            case R.id.ibtn_closeDialog:
                this.dismiss();
                break;
                default:break;
        }
    }
    private String tangSoLuong(){
        soLuong+=1;
        return String.valueOf(soLuong);
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    private String giamSoLuong(){
        soLuong=max(0,soLuong-1);
        return String.valueOf(soLuong);
    }
    private void updateSoLuong(String soluong){
        tv_soLuongSanPhamDialog.setText(soluong);
    }

    private void changeBackgroundButton(int soLuong){
        if(soLuong<=0) {
            btn_capNhatSanPham.setBackground(ResourcesCompat.getDrawable(context.getResources(),R.drawable.background_button_delete,null));
            btn_capNhatSanPham.setText("Xóa");
            checkCode=0;
        }else {
            btn_capNhatSanPham.setBackground(ResourcesCompat.getDrawable(context.getResources(),R.drawable.background_button_datdon,null));
            btn_capNhatSanPham.setText("Cập nhật sản phẩm");
            checkCode=1;
        }
    }
}
