package fpo.thanh.coffeeshop.domain.Room.RoomModel;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ThucDonDaDat {
    @PrimaryKey(autoGenerate = true)
    int _id;
    @ColumnInfo(name = "maThucDon")
    int maThucDon;
    @ColumnInfo(name = "tenThucDon")
    String tenThucDon;
    @ColumnInfo(name = "idHoaDon")
    int idHoaDon;
    @ColumnInfo(name = "idHoaDonLocal")
    String idHoaDonLocal;
    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getMaThucDon() {
        return maThucDon;
    }

    public void setMaThucDon(int maThucDon) {
        this.maThucDon = maThucDon;
    }

    public String getTenThucDon() {
        return tenThucDon;
    }

    public void setTenThucDon(String tenThucDon) {
        this.tenThucDon = tenThucDon;
    }

    public int getIdHoaDon() {
        return idHoaDon;
    }

    public void setIdHoaDon(int idHoaDon) {
        this.idHoaDon = idHoaDon;
    }

    public String getIdHoaDonLocal() {
        return idHoaDonLocal;
    }

    public void setIdHoaDonLocal(String idHoaDonLocal) {
        this.idHoaDonLocal = idHoaDonLocal;
    }

}
