package fpo.thanh.coffeeshop.domain.Room.Callback;

import java.util.List;

import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;

public interface ListDsMonCallbackInterface {
    interface View{
        void resultDsMon(List<DsMon> result);
    }
    void requestDsMon(String maHoaDonLocal);
}
