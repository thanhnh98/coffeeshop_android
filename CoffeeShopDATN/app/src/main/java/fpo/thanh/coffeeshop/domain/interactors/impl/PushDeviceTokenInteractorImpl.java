package fpo.thanh.coffeeshop.domain.interactors.impl;

import fpo.thanh.coffeeshop.domain.executor.Executor;
import fpo.thanh.coffeeshop.domain.executor.MainThread;
import fpo.thanh.coffeeshop.domain.interactors.PushDeviceTokenInteractor;
import fpo.thanh.coffeeshop.domain.interactors.base.AbstractInteractor;
import fpo.thanh.coffeeshop.domain.repository.PushDeviceTokenRepository;
import fpo.thanh.coffeeshop.shareModel.PushTokenModel;
import kotlin.collections.AbstractIterator;

public class PushDeviceTokenInteractorImpl extends AbstractInteractor implements PushDeviceTokenInteractor {
    private final Callback callback;
    private final String token;
    private final PushDeviceTokenRepository repository;

    public PushDeviceTokenInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback callback, String token, PushDeviceTokenRepository repository) {
        super(threadExecutor, mainThread);
        this.callback = callback;
        this.token = token;
        this.repository = repository;
    }

    @Override
    public void run() {
        final PushTokenModel result=repository.pushTokenDevice(token);
        mMainThread.post(()->{
            callback.onFinish(result);
        });
    }
}
