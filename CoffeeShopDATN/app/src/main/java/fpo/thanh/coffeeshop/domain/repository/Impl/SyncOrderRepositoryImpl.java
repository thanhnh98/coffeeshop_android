package fpo.thanh.coffeeshop.domain.repository.Impl;

import android.util.Log;

import fpo.thanh.coffeeshop.domain.repository.SyncOrderRepository;
import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;
import fpo.thanh.coffeeshop.storage.network.RestClient;
import fpo.thanh.coffeeshop.storage.service.SyncOrderService;
import retrofit2.Response;

public class SyncOrderRepositoryImpl implements SyncOrderRepository {
    @Override
    public SyncOrderModel syncOrder(int maHoaDon, String maHoaDonLocal, String maNhanVienOrder, String thoiGianLap, int maBan, String tenBan, String tokenDevice, Double tongTien, int giamGia, Double thanhTien, String dsMonJs,int trangThai) {
        SyncOrderService service= RestClient.createService(SyncOrderService.class);
        try{
            Response<SyncOrderModel> response=service.syncOrder(maHoaDon,maHoaDonLocal,maNhanVienOrder,thoiGianLap,maBan,tenBan,tokenDevice,tongTien,giamGia,thanhTien,dsMonJs,trangThai).execute();
            if(response.isSuccessful()){
                return response.body();
            }
        }catch (Exception e){
            Log.e("SyncOrder",e.getMessage());
        }
        return null;
    }
}
