package fpo.thanh.coffeeshop.presentation.presenter;

import fpo.thanh.coffeeshop.shareModel.PushTokenModel;

public interface PushDeviceTokenPresenter {
    interface View{
       void showResultPushToken(PushTokenModel result);
    }
    void requestPushToken(String token);
}
