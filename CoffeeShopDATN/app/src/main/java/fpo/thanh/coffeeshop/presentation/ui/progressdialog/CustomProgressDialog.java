package fpo.thanh.coffeeshop.presentation.ui.progressdialog;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;

import fpo.thanh.coffeeshop.R;

public class CustomProgressDialog extends AlertDialog {
    public CustomProgressDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert_progress);
    }

}
