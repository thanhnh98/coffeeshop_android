package fpo.thanh.coffeeshop.presentation.ui.listeners;

import android.content.Context;

public class RefreshListTableCallbackImpl implements RefreshListTableCallback {
    private final Context context;
    private final Callback callback;

    public RefreshListTableCallbackImpl(Context context, Callback callback){

        this.context = context;
        this.callback = callback;
    }
    @Override
    public void requestRefreshList() {
        callback.onRefreshListTable();
    }
}
