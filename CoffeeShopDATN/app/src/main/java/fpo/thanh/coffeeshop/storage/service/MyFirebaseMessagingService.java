package fpo.thanh.coffeeshop.storage.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Handler;

import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.domain.Room.AppDatabase;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.NotificationModel;
import fpo.thanh.coffeeshop.domain.executor.impl.ThreadExecutor;
import fpo.thanh.coffeeshop.domain.repository.Impl.GetOrderRepositoryImpl;
import fpo.thanh.coffeeshop.domain.repository.Impl.PushDeviceTokenRepositoryImpl;
import fpo.thanh.coffeeshop.presentation.presenter.GetOrderPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.PushDeviceTokenPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.impl.GetOrderPresenterImpl;
import fpo.thanh.coffeeshop.presentation.presenter.impl.PushDeviceTokenPresenterImpl;
import fpo.thanh.coffeeshop.presentation.ui.activity.MainActivity;
import fpo.thanh.coffeeshop.presentation.ui.fragment.ListTableFragment;
import fpo.thanh.coffeeshop.shareModel.DsMonModel;
import fpo.thanh.coffeeshop.shareModel.GetOrderModel;
import fpo.thanh.coffeeshop.shareModel.HoaDonModel;
import fpo.thanh.coffeeshop.shareModel.PushTokenModel;
import fpo.thanh.coffeeshop.storage.savePreference.CacheClient;
import fpo.thanh.coffeeshop.threading.MainThreadImpl;

public class MyFirebaseMessagingService extends FirebaseMessagingService implements GetOrderPresenter.View,PushDeviceTokenPresenter.View {
    private static final String CHANNEL_ID = "TLE";
    AppDatabase mDb;
    NotificationModel notificationModel;
    PushDeviceTokenPresenter presenter;
    GetOrderPresenter presenterGetOrder;
    String message="";
    @Override
    public void onCreate() {
        super.onCreate();
        notificationModel=new NotificationModel();
        mDb=AppDatabase.getDatabase(this);
        presenter=new PushDeviceTokenPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),
                this,new PushDeviceTokenRepositoryImpl());
        presenterGetOrder=new GetOrderPresenterImpl(ThreadExecutor.getInstance(),MainThreadImpl.getInstance(),
                this,new GetOrderRepositoryImpl());
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.e("FIREBASE", "From: " + remoteMessage.getFrom());
        Log.e("ID",getIdFromMessage(remoteMessage.getData()+""));
        // Check if message contains a data payload.
        //Toast.makeText(this, getMessage(remoteMessage.getData()+""), Toast.LENGTH_SHORT).show();
        if (remoteMessage.getData().size() > 0) {
            Log.e("FIREBASE", "Message data payload: " + remoteMessage.getData());

            createNotificationChannel();
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle("Đã gửi yêu cầu")
                    .setContentText(getMessage(remoteMessage.getData()+""))
                    .setColor(Color.RED)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            insertNewNotification(getMessage(remoteMessage.getData()+""));
            updateHoaDon(remoteMessage.getData()+"");
            message=getMessage(remoteMessage.getData()+"");

            synchronized (builder){
                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

// notificationId is a unique int for each notification that you must define
                notificationManager.notify(0, builder.build());
            }



            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
               // scheduleJob();
            } else {
                // Handle message within 10 seconds
               // handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e("FIREBASE", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        CacheClient.setCache(this,"tokenfirebase_real",s);
        presenter.requestPushToken(s);
        Log.e("New Token Firebase",s);
    }
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Log.e("OKK",CHANNEL_ID);
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
    private String getMessage(String s){
        String tmp="";
        for(int i=54;i<s.length();++i){
            if(s.charAt(i)==',') break;
            else tmp+=s.charAt(i);
        }
        return tmp;
    }
    private void insertNewNotification(String content){
        notificationModel.setContent(content);
        notificationModel.setTimeGenerate(getCurrentTime());
        notificationModel.setHasRead(0);
        mDb.notificationDao().insertNotification(notificationModel);
    }
    String getCurrentTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date date = new Date(System.currentTimeMillis());
        String curTime = String.valueOf(formatter.format(date));
        return curTime;
    }
    String getLocalId(String s){
        String tmp="";
        tmp=s.substring(26,44);
        Log.e("Local ID From message",tmp);
        return tmp;
    }
    Integer getStatus(String s){
            String tmp="";
            for(int i=8;i<s.length();++i){
                if(s.charAt(i)==',') break;
                else tmp+=s.charAt(i);
            }
            Log.e("stt from message",tmp);
            return Integer.parseInt(tmp);
    }
    void updateHoaDon(String s){
        /*getLocalId(s);
        HoaDon hoaDon=null;
        hoaDon=mDb.hoaDonDAO().getHoaDonByMaHoaDonLocal(getLocalId(s));
        if(hoaDon==null){

        }else {
            hoaDon.setMaHoaDonLocal(getLocalId(s));
            hoaDon.setTrangThai(getStatus(s));
        }
        Log.e("chi teit hoa don",new Gson().toJson(hoaDon));
        mDb.hoaDonDAO().insertHoaDon(hoaDon);*/
        presenterGetOrder.requestGetOrder(getIdFromMessage(s));

    }

    @Override
    public void showResultPushToken(PushTokenModel result) {
        try {
            if (result.getExitCode() == 1) {
                Log.e("Push thanhf coong", result.getMessage());
            } else {
                Log.e("push failed", "Fail");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    String getIdFromMessage(String s){
            //{status=1, order_local_id=157577966534916088, message=TR07-Tầng trệt có order mới, order_id=1252, created_date=2019-12-08 11:34:25}

        return s.substring(s.length()-39,s.length()-35);
    }

    @Override
    public void showResultGetOrder(GetOrderModel result) {
        try{
            if(result.getExitCode()==1){
                Log.e("Nhanja","Susscess");
                HoaDonModel serverResult=result.getData();
                Log.e("id hoa don local",result.getData().getMaHoaDonLocal());
                HoaDon hoaDon=new HoaDon();
                hoaDon.setTrangThai(serverResult.getTrangThai());
                hoaDon.setMaHoaDonLocal(serverResult.getMaHoaDonLocal());
                hoaDon.setMaHoaDon(serverResult.getMaHoaDon());
                hoaDon.setTenNhanVienOrder(serverResult.getTenNhanVienOrder());
                hoaDon.setHasSync(1);
                hoaDon.setMaBan(serverResult.getMaBan());
                hoaDon.setThoiGianLap(serverResult.getThoiGianLap());
                hoaDon.setMaThuNgan(serverResult.getMaThuNgan());
                hoaDon.setTenBan(serverResult.getTenBan());
                hoaDon.setThanhTien(serverResult.getThanhTien());
                hoaDon.setTongTien(serverResult.getTongTien());
                hoaDon.setMaNhanVien(serverResult.getMaNhanVienOrder());
                hoaDon.setGiamGia(serverResult.getGiamGia());
                mDb.hoaDonDAO().insertHoaDon(hoaDon);
                DsMon dsMon=new DsMon();
                for(DsMonModel iDsMon:result.getData().getDsMon()){
                    dsMon.setMaHoaDon(iDsMon.getMaHoaDon());
                    dsMon.setMaHoaDonLocal(hoaDon.getMaHoaDonLocal());
                    dsMon.setTenThucDon(iDsMon.getTenThucDon());
                    dsMon.setTrangThai(iDsMon.getTrangThai());
                    dsMon.setSoLuong(iDsMon.getSoLuong());
                    dsMon.setMaThucDon(iDsMon.getMaThucDon());
                    dsMon.setMaChiTiet(iDsMon.getMaChiTiet());
                    dsMon.setDonGia(iDsMon.getDonGia());
                    dsMon.setGiaMon(iDsMon.getGiaMon());
                    dsMon.setMaChiTietLocal(iDsMon.getMaChiTietLocal());
                    mDb.dsMonDAO().insertDsMon(dsMon);
                }
                ListTableFragment.RefreshMainActivity refreshMainActivity =new ListTableFragment.RefreshMainActivity();
            refreshMainActivity.execute(message);
            }else {

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
