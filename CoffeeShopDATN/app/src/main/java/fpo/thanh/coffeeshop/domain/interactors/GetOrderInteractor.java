package fpo.thanh.coffeeshop.domain.interactors;

import fpo.thanh.coffeeshop.domain.interactors.base.Interactor;
import fpo.thanh.coffeeshop.shareModel.GetOrderModel;

public interface GetOrderInteractor extends Interactor {
    interface Callback{
        void onFinish(GetOrderModel result);
    }
}
