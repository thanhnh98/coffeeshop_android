package fpo.thanh.coffeeshop.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.contanst.CacheKey;
import fpo.thanh.coffeeshop.contanst.LoaiThucDon;
import fpo.thanh.coffeeshop.domain.Room.AppDatabase;
import fpo.thanh.coffeeshop.presentation.ui.fragment.DetailSingleOrderFragment;
import fpo.thanh.coffeeshop.presentation.ui.fragment.ListThucDocFragment;
import fpo.thanh.coffeeshop.presentation.ui.listeners.RefreshListTableCallback;

public class SupportActivity extends AppCompatActivity implements View.OnClickListener {
    String tenBan;
    int maTang,maBan;
    @BindView(R.id.toolBarSupportActivity)
    Toolbar toolbar;
    @BindView(R.id.tv_tenTang)
    TextView tv_tenTang;
    int TYPE_REQUEST;
    public AppDatabase mDB;
    int addSign=0;
    ListThucDocFragment fragment;
    String maHoaDonLocal;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holderfragment);
        ButterKnife.bind(this);

        mDB=AppDatabase.getDatabase(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        tenBan=getIntent().getExtras().getString("tenBan");
        maTang=getIntent().getExtras().getInt("maTang");
        maBan=getIntent().getExtras().getInt("maBan");
        addSign=getIntent().getExtras().getInt("addSign",0);
        maHoaDonLocal=getIntent().getExtras().getString("maHoaDonLocal");
        TYPE_REQUEST=getIntent().getExtras().getInt("requestCode",0);

        if(TYPE_REQUEST==0){
            tv_tenTang.setText(tenBan+" - "+ CacheKey.getTenTang(maTang));
            fragment=ListThucDocFragment.getInstance(maBan,maTang,tenBan,addSign,maHoaDonLocal);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.layout_holder,fragment)
                    .addToBackStack("holder")
                    .commit();
        }else if(TYPE_REQUEST==1){
            tv_tenTang.setText("Chi tiết hóa đơn");
            //fragment=ListThucDocFragment.getInstance(maBan,maTang,tenBan);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.layout_holder, DetailSingleOrderFragment.getInstance(maHoaDonLocal))
                    .addToBackStack("holder")
                    .commit();
        }


    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()>1) getSupportFragmentManager().popBackStack();
        else this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Log.e("Close Support DB","close");
        this.getSupportFragmentManager().getFragments().clear();
        //mDB.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // getMenuInflater().inflate(R.menu.menu_sort,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
                default:break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
//            case R.id.coffee:
//                fragment.getDsByLoaiThucDon(LoaiThucDon.Coffee.getText());
//                break;
//            case R.id.drink:
//                fragment.getDsByLoaiThucDon(LoaiThucDon.Drink.getText());
//
//                break;
//            case R.id.fastFood:
//                fragment.getDsByLoaiThucDon(LoaiThucDon.Fast_Food.getText());
//                break;
//            case R.id.others:
//                fragment.getDsByLoaiThucDon(LoaiThucDon.Others.getText());
//                break;
//            case R.id.daXay:
//                fragment.getDsByLoaiThucDon(LoaiThucDon.DaXay.getText());
//                break;
//            case R.id.freeze:
//                fragment.getDsByLoaiThucDon(LoaiThucDon.Freeze.getText());
//                break;
//            case R.id.all:
//                fragment.getAllThucDon();
//                break;
//            case R.id.giaThapDenCao:
//                fragment.sapXepGiaThapDenCao();
//                break;
//            case R.id.giaCaoDenThap:
//                fragment.sapXepGiaCaoDenThap();
//                break;
                default:break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
