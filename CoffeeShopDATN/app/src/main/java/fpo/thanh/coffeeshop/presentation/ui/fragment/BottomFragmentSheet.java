package fpo.thanh.coffeeshop.presentation.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bluejamesbond.text.DocumentView;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fpo.thanh.coffeeshop.R;

public class BottomFragmentSheet extends BottomSheetDialogFragment {
    private static ListThucDocFragment fragment;
    private static int maThucDon;
    private String tenSP="";
    private Double giaSP=0.0;
    private String moTa="";
    private int maBan,maTang;
    private int soLuongSanPham=1;
    @BindView(R.id.ibtn_giamSanPhamBottomSheet)
    ImageButton ibtn_giamSanPham;
    @BindView(R.id.ibtn_tangSanPhamBottomSheet)
    ImageButton ibtnTangSanPham;
    @BindView(R.id.tv_giaSanPhamBottomSheet)
    TextView tv_giaSanPham;
    @BindView(R.id.tv_tenSanPhamBottomSheet)
    TextView tv_tenSanPham;
    @BindView(R.id.tv_soLuongSanPham)
    TextView tv_soLuongSanPham;
    @BindView(R.id.btn_themVaoGioHangBottomSheet)
    Button btn_themVaoGioHang;
   @BindView(R.id.dv_mieuTa)
    DocumentView dv_mieuTa;
    private static BottomFragmentSheet bottomFragmentSheet=null;
    public static BottomFragmentSheet getInstance(ListThucDocFragment fragment,int maThucDon,String tenSP,Double giaSP,String moTa,int maBan,int maTang){
        BottomFragmentSheet.fragment = fragment;
        BottomFragmentSheet.maThucDon = maThucDon;
        bottomFragmentSheet=new BottomFragmentSheet();
        Bundle bundle=new Bundle();
        bundle.putString("tenSP",tenSP);
        bundle.putDouble("giaSP",giaSP);
        bundle.putString("moTa",moTa);
        bundle.putInt("maBan",maBan);
        bundle.putInt("maTang",maTang);
        bottomFragmentSheet.setArguments(bundle);
        return bottomFragmentSheet;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tenSP=getArguments().getString("tenSP");
        giaSP=getArguments().getDouble("giaSP");
        moTa=getArguments().getString("moTa");
        maBan=getArguments().getInt("maBan");
        maTang=getArguments().getInt("maTang");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bottomsheet_detailproduct,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        capNhatChuTrenNutThanhToan(1);
        tv_tenSanPham.setText(tenSP);
        tv_giaSanPham.setText(""+giaSP+"$");
        dv_mieuTa.setText(moTa);
        btn_themVaoGioHang.setOnClickListener(v->{
            this.dismiss();
            Toast.makeText(getActivity(), "Thêm vài giỏ hàng thành công", Toast.LENGTH_SHORT).show();
            fragment.updateValueHashDsSanPham(maThucDon,soLuongSanPham);
        });
    }
    @OnClick(R.id.ibtn_tangSanPhamBottomSheet)
    void tangSanPham(){
        soLuongSanPham++;
        tv_soLuongSanPham.setText(""+soLuongSanPham);
        capNhatChuTrenNutThanhToan(soLuongSanPham);
    }
    @OnClick(R.id.ibtn_giamSanPhamBottomSheet)
    void giamSanPham(){
        soLuongSanPham=Math.max(1,soLuongSanPham-1);
        tv_soLuongSanPham.setText(""+soLuongSanPham);
        capNhatChuTrenNutThanhToan(soLuongSanPham);
    }
    private void capNhatChuTrenNutThanhToan(int soLuongSanPham){
        btn_themVaoGioHang.setText(getResources().getString(R.string.chonmonnay)+" - "+(giaSP*soLuongSanPham)+"$");
    }
    
}
