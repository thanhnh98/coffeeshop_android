package fpo.thanh.coffeeshop.presentation.ui.listeners;

public interface RefreshListTableCallback {
    interface Callback{
        void onRefreshListTable();
    }
    void requestRefreshList();
}
