package fpo.thanh.coffeeshop.presentation.presenter;

import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;

public interface SyncOrderPresenter  {
    interface View{
        void onShowResultSyncOrder(SyncOrderModel result);
    }
    void requestSyncOrder(int maHoaDon, String maHoaDonLocal, String maNhanVienOrder, String thoiGianLap,
                          int maBan, String tenBan, String tokenDevice, Double tongTien,
                          int giamGia, Double thanhTien, String dsMonJs,int trangThai);
}
