package fpo.thanh.coffeeshop.presentation.ui.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.contanst.STATUS_ORDER;
import fpo.thanh.coffeeshop.domain.Room.Callback.ListDsMonCallback;
import fpo.thanh.coffeeshop.domain.Room.Callback.ListDsMonCallbackInterface;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.BanAn;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.domain.executor.impl.ThreadExecutor;
import fpo.thanh.coffeeshop.domain.repository.Impl.SyncOrderRepositoryImpl;
import fpo.thanh.coffeeshop.presentation.presenter.SyncOrderPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.impl.SyncOrderPresenterImpl;
import fpo.thanh.coffeeshop.presentation.ui.activity.MainActivity;
import fpo.thanh.coffeeshop.presentation.ui.activity.SupportActivity;
import fpo.thanh.coffeeshop.presentation.ui.adapter.ListDetailSingleOrderAdapter;
import fpo.thanh.coffeeshop.shareModel.PostChiTietModel;
import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;
import fpo.thanh.coffeeshop.storage.savePreference.CacheClient;
import fpo.thanh.coffeeshop.threading.MainThreadImpl;

public class DetailSingleOrderFragment extends Fragment implements ListDsMonCallbackInterface.View, SyncOrderPresenter.View {

    private String maHodDonLocal;
    ListDsMonCallback getDsMonCallback;
    SupportActivity activity;
    @BindView(R.id.tv_status)
    TextView tv_status;
    @BindView(R.id.rcv_detailSingleOrder)
    RecyclerView rcv_detailSingleOrder;
    @BindView(R.id.tv_totalPrize)
    TextView tv_totalPrize;
    @BindView(R.id.btn_thanhToan)
    Button btn_thanhToan;
    @BindView(R.id.btn_huyHoaDon)
    Button btn_huyHoaDon;
    @BindView(R.id.btn_themMon)
    Button btn_themMon;
    HoaDon hoaDon;
    BanAn banAn;
    ProgressDialog progressDialog;
    SyncOrderPresenter presenter;
    List<DsMon> listData=new ArrayList<>();
    ListDetailSingleOrderAdapter adapter;
    private final int status_huy=3;
    private final int status_thanhtoan=8;
    private static DetailSingleOrderFragment fragment;
    public static DetailSingleOrderFragment getInstance(String maHoaDonLocal){
        fragment=new DetailSingleOrderFragment();
        Bundle bundle=new Bundle();
        bundle.putString("maHoaDonLocal",maHoaDonLocal);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        maHodDonLocal=getArguments().getString("maHoaDonLocal");
        Log.e("maHoaDonLocal",maHodDonLocal);
        getDsMonCallback=new ListDsMonCallback(getActivity(),activity.mDB,this, MainThreadImpl.getInstance());
        hoaDon=activity.mDB.hoaDonDAO().getHoaDonByMaHoaDonLocal(maHodDonLocal);
        banAn=activity.mDB.banAnDAO().getBanAnByMaBan(hoaDon.getMaBan());
        adapter=new ListDetailSingleOrderAdapter(listData);
        adapter.notifyDataSetChanged();
        progressDialog=new ProgressDialog(activity);
        presenter=new SyncOrderPresenterImpl(ThreadExecutor.getInstance(),MainThreadImpl.getInstance(),this,new SyncOrderRepositoryImpl());

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail_single_order,container,false);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        btn_huyHoaDon.setOnClickListener(v->{
            new AlertDialog.Builder(activity)
                    .setMessage("Xac nhận hủy hóa đơn")
                    .setPositiveButton("Hủy", (dialog,which)->{
                        dialog.dismiss();
                        presenter.requestSyncOrder(hoaDon.getMaHoaDon(),hoaDon.getMaHoaDonLocal(), CacheClient.getCache(activity,"maNhanVien"),hoaDon.getThoiGianLap(),
                                hoaDon.getMaBan(),hoaDon.getTenBan(),CacheClient.getCache(activity,"tokenfirebase"),hoaDon.tongTien,0,
                                hoaDon.thanhTien,convertJsonFromListDsMon(activity.mDB.dsMonDAO().getDsMonByMaHoadonLocal(hoaDon.getMaHoaDonLocal())),status_huy);
                        progressDialog.setMessage("Đang xử lý");
                        progressDialog.show();
                    })
                    .setNegativeButton("Đóng",(dialog,which)->{
                        dialog.dismiss();
                    }).show();

        });
        btn_thanhToan.setOnClickListener(v->{
            new AlertDialog.Builder(activity)
                    .setMessage("Xac nhận thanh toán")
                    .setPositiveButton("Thanh toán", (dialog,which)->{
                        dialog.dismiss();
                        presenter.requestSyncOrder(hoaDon.getMaHoaDon(),hoaDon.getMaHoaDonLocal(), CacheClient.getCache(activity,"maNhanVien"),hoaDon.getThoiGianLap(),
                                hoaDon.getMaBan(),hoaDon.getTenBan(),CacheClient.getCache(activity,"tokenfirebase"),hoaDon.tongTien,0,
                                hoaDon.thanhTien,convertJsonFromListDsMon(activity.mDB.dsMonDAO().getDsMonByMaHoadonLocal(hoaDon.getMaHoaDonLocal())),status_thanhtoan);
                        progressDialog.setMessage("Đang xử lý");
                        progressDialog.show();
                    })
                    .setNegativeButton("Đóng",(dialog,which)->{
                        dialog.dismiss();
                    }).show();

        });
        btn_themMon.setOnClickListener(v->{
            Intent intent = new Intent(activity, SupportActivity.class);
            intent.putExtra("tenBan", hoaDon.getTenBan());
            intent.putExtra("maTang",banAn.maTang);
            intent.putExtra("maBan", hoaDon.getMaBan());
            intent.putExtra("requestCode", 0);
            intent.putExtra("addSign",1);
            intent.putExtra("maHoaDonLocal",hoaDon.getMaHoaDonLocal());
            //context.startActivity(intent);
            activity.startActivity(intent);
        });
        if(hoaDon.getTrangThai()==6||hoaDon.getTrangThai()==7){
            btn_huyHoaDon.setVisibility(View.GONE);
        }
        if(hoaDon.getTrangThai()==status_huy||hoaDon.getTrangThai()==status_thanhtoan){
            btn_thanhToan.setVisibility(View.GONE);
            btn_huyHoaDon.setVisibility(View.GONE);
        }
        tv_status.setText(STATUS_ORDER.STATUS_ORDER(hoaDon.getTrangThai()));
        setStatusStyle(tv_status, hoaDon.getTrangThai());

    }

    @Override
    public void resultDsMon(List<DsMon> result) {
        try{
            double prizeTotal=0;
            listData.clear();
            listData.addAll(result);
            for(DsMon dsMon:result) prizeTotal+=dsMon.donGia;
            tv_totalPrize.setText(String.valueOf(prizeTotal)+"$");
            rcv_detailSingleOrder.setLayoutManager(new LinearLayoutManager(activity));
            rcv_detailSingleOrder.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity=(SupportActivity)context;
    }

    @Override
    public void onShowResultSyncOrder(SyncOrderModel result) {
        try{
            progressDialog.dismiss();
            if(result.getExitCode()==1){
                switch (result.getData().getTrangThai()){
                    case status_huy:
                        Toast.makeText(activity, "Hủy hóa đơn thành công", Toast.LENGTH_SHORT).show();
                        hoaDon.setTrangThai(status_huy);
                        activity.mDB.hoaDonDAO().insertHoaDon(hoaDon);
                        activity.finish();
                        break;
                    case status_thanhtoan:
                        Toast.makeText(activity, "Thanh toán thành công", Toast.LENGTH_SHORT).show();
                        hoaDon.setTrangThai(status_thanhtoan);
                        activity.mDB.hoaDonDAO().insertHoaDon(hoaDon);
                        activity.finish();
                        break;
                }
            }
        }catch (Exception e){
            Toast.makeText(activity, "Có lỗi trong qua trình xử lý", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }
    }
    private String convertJsonFromListDsMon(List<DsMon> dsMons){
        String json="";
        Log.e("size",dsMons.size()+"");

        json="[";
        Gson gson=new Gson();
        for(DsMon dsMon:dsMons){
            Log.e("tesst convertjson", dsMon.getMaChiTietLocal());
            json+=gson.toJson(new PostChiTietModel(0,dsMon.getMaHoaDon(),dsMon.getMaHoaDonLocal(),dsMon.getMaThucDon(),
                    dsMon.getSoLuong(),dsMon.getDonGia(),dsMon.getMaChiTietLocal(),dsMon.getTrangThai()));
            json+=",";
        }
        StringBuilder resultJson=new StringBuilder(json);
        if(resultJson.length()>1)
            resultJson.setCharAt(json.length()-1,']');
        else return json+']';
        Log.e("Json peding",resultJson.toString());
        return resultJson.toString();
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setStatusStyle(TextView tv, int s) {
        switch (s) {
            case 1: { //new (hóa đơn mới)
                //Log.e("??","sao ko dodoir mauf ta");
                tv.setTextColor(activity.getResources().getColor(R.color.status_pending, null));
                break;
            }
            case 2: { //update (đã cập nhật)
                tv.setTextColor(activity.getResources().getColor(R.color.status_capnhat, null));
                break;
            }
            case 3: { //cancel (đã hủy)
                tv.setTextColor(activity.getResources().getColor(R.color.status_huy, null));
                break;
            }
            case 4: { //change ignore
                tv.setTextColor(activity.getResources().getColor(R.color.status_pending, null));
                break;
            }
            case 5: { //merge
                tv.setTextColor(activity.getResources().getColor(R.color.status_pending, null));
                break;
            }
            case 6: { //cooking (đang chế biến)
                tv.setTextColor(activity.getResources().getColor(R.color.status_cooking, null));
                break;
            }
            case 7: { //cooked (đã trả món)
                tv.setTextColor(activity.getResources().getColor(R.color.status_cooked, null));
                break;
            }
            case 8: { //complete (đã thanh toán)
                tv.setTextColor(activity.getResources().getColor(R.color.status_completed, null));
                break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getDsMonCallback.requestDsMon(maHodDonLocal);
    }
}
