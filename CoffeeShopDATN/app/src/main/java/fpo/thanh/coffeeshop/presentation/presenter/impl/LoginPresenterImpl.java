package fpo.thanh.coffeeshop.presentation.presenter.impl;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;

import fpo.thanh.coffeeshop.domain.executor.Executor;
import fpo.thanh.coffeeshop.domain.executor.MainThread;
import fpo.thanh.coffeeshop.domain.executor.impl.ThreadExecutor;
import fpo.thanh.coffeeshop.domain.interactors.LoginInteractor;
import fpo.thanh.coffeeshop.domain.interactors.impl.LoginInteractorImpl;
import fpo.thanh.coffeeshop.domain.repository.LoginRepository;
import fpo.thanh.coffeeshop.presentation.presenter.AbstractPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.LoginPresenter;
import fpo.thanh.coffeeshop.shareModel.LoginModel;
import fpo.thanh.coffeeshop.threading.MainThreadImpl;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, LoginInteractor.Callback {
    private final Context context;
    private final View view;
    private final LoginRepository repository;
    ProgressDialog dialog;
    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context context, View view, LoginRepository repository) {
        super(executor, mainThread);
        this.context = context;
        this.view = view;
        this.repository = repository;
        dialog =new ProgressDialog(context);

    }

    @Override
    public void onFinish(LoginModel result) {
        try {
            view.showResultLogin(result);
            hideProgress();
            if (result.getExitCode() == 0) {
                showError("Đăng nhập thất bại");
            }
        }catch (Exception e){
            showError("Đăng nhập thất bại");
        }
    }

    @Override
    public void requestLogin(String username, String password) {
        LoginInteractor interactor=new LoginInteractorImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),
                this,repository,username,password);
        interactor.execute();
        showProgress();
    }

    @Override
    public void showProgress() {
        dialog.setMessage("Đang đăng nhập...");
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void showError(String message) {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.show();
    }
}
