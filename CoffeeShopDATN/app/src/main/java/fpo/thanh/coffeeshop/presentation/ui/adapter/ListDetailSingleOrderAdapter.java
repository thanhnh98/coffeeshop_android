package fpo.thanh.coffeeshop.presentation.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;

public class ListDetailSingleOrderAdapter extends RecyclerView.Adapter<ListDetailSingleOrderAdapter.ViewHolder> {
    private List<DsMon> listData;

    public ListDetailSingleOrderAdapter(List<DsMon> listData){

        this.listData = listData;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listdetailsingleorder,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_giaSP.setText(String.valueOf(listData.get(position).getGiaMon()*listData.get(position).getSoLuong())+"$");
        holder.tv_nameDetailSingle.setText(listData.get(position).getTenThucDon());
        holder.tv_soLuongDetailSingle.setText("x"+listData.get(position).getSoLuong());
        if(listData.get(position).getTrangThai()==7)
            holder.tv_statusThucDon.setVisibility(View.VISIBLE);
        else holder.tv_statusThucDon.setVisibility(View.GONE);

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_giaSP)
        TextView tv_giaSP;
        @BindView(R.id.tv_nameDetailSingle)
        TextView tv_nameDetailSingle;
        @BindView(R.id.tv_soLuongDetailSingle)
        TextView tv_soLuongDetailSingle;
        @BindView(R.id.tv_statusThucDon)
        TextView tv_statusThucDon;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
