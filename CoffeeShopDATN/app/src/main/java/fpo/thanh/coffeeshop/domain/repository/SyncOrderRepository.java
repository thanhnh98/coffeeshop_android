package fpo.thanh.coffeeshop.domain.repository;

import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;
import fpo.thanh.coffeeshop.storage.service.SyncOrderService;

public interface SyncOrderRepository {
    SyncOrderModel syncOrder(int maHoaDon,String maHoaDonLocal,String maNhanVienOrder,String thoiGianLap,int maBan,
                             String tenBan,String tokenDevice,Double tongTien,int giamGia,Double thanhTien,String dsMonJs,int trangThai);
}
