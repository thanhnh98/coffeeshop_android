package fpo.thanh.coffeeshop.shareModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("exit_code")
    @Expose
    private Integer exitCode;
    @SerializedName("data")
    @Expose
    private DataLoginModel data;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getExitCode() {
        return exitCode;
    }

    public void setExitCode(Integer exitCode) {
        this.exitCode = exitCode;
    }

    public DataLoginModel getData() {
        return data;
    }

    public void setData(DataLoginModel data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
