package fpo.thanh.coffeeshop.presentation.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.contanst.CacheKey;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.presentation.ui.activity.MainActivity;
import fpo.thanh.coffeeshop.presentation.ui.adapter.ListHoaDonCompleteAdapter;
import fpo.thanh.coffeeshop.presentation.ui.adapter.ListHoaDonPedingAdapter;
import fpo.thanh.coffeeshop.storage.savePreference.CacheClient;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ListBookedFragment extends Fragment {

    List<HoaDon> listData;
    ListHoaDonCompleteAdapter adapter=null;
    MainActivity activity;
    List<HoaDon> listDataTmp; // this list to control

    @BindView(R.id.rcv_listHoaDonBooked)
    RecyclerView rcv_listBooked;
    @BindView(R.id.swipteRefresh)
    SwipeRefreshLayout swipteRefresh;
    @BindView(R.id.tv_holderBooked)
    TextView tv_holderBooked;
    static int CHECK_UPDATE_LIST=0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_booked,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listData=new ArrayList<>();
        listDataTmp=new ArrayList<>();
        adapter=new ListHoaDonCompleteAdapter(activity,listData);
        listDataTmp=activity.mDB.hoaDonDAO().getHoaDonSynced();
        Collections.reverse(listDataTmp);
        listData.addAll(listDataTmp);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        setupRecyclerView();
        swipteRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                listData.clear();
//                listData.addAll(activity.mDB.hoaDonDAO().getHoaDonSynced());
//                Log.e("size completed",listData.size()+"");
//                adapter.notifyDataSetChanged();
                listDataTmp=activity.mDB.hoaDonDAO().getHoaDonSynced();
                    Collections.reverse(listDataTmp);
                    listData.clear();
                    listData.addAll(listDataTmp);
                    adapter.notifyDataSetChanged();
                if(listData.size()==0){
                    tv_holderBooked.setVisibility(View.INVISIBLE);
                }
                else tv_holderBooked.setVisibility(View.GONE);
                swipteRefresh.setRefreshing(false);
            }
        });
        autoUpdate();
    }
    @Override
    public void onResume() {
        super.onResume();
        if(adapter!=null)
            updateList();
    }
    private void setupRecyclerView(){
        rcv_listBooked.setAdapter(adapter);
        rcv_listBooked.setLayoutManager(new LinearLayoutManager(activity));
        adapter.notifyDataSetChanged();
    }
    private void updateList(){
        listDataTmp=activity.mDB.hoaDonDAO().getHoaDonSynced();
        if(listData.size()!=listDataTmp.size()){
            Collections.reverse(listDataTmp);
            listData.clear();
            listData.addAll(listDataTmp);
            adapter.notifyDataSetChanged();
        }
        if(listData.size()==0){
            tv_holderBooked.setVisibility(View.INVISIBLE);
        }
        else tv_holderBooked.setVisibility(View.GONE);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity=(MainActivity)context;
    }
    void autoUpdate(){
        Observable.just(1)
                //.debounce(3000,TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .repeatWhen ((completed)->completed.delay(5, TimeUnit.SECONDS))
                .subscribe(
                        next -> {
                            try {
                                if(adapter!=null){
                                    listDataTmp=activity.mDB.hoaDonDAO().getHoaDonSynced();
                                    if(listData.size() !=listDataTmp.size()){
                                        activity.runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {
                                                //Log.e("update cooked nè","OK update nè");
                                                Collections.reverse(listDataTmp);
                                                listData.clear();
                                                listData.addAll(listDataTmp);
                                                adapter.notifyItemInserted(0);
                                                rcv_listBooked.smoothScrollToPosition(0);

                                            }
                                        });


                                    }
                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                            //Log.e(listHoaDonPending.get(1).getMaHoaDonLocal(),convertJsonFromListDsMon(mDB.dsMonDAO().getDsMonByMaHoadonLocal(listHoaDonPending.get(1).getMaHoaDonLocal())));
                            // if()

                        },
                        error -> error.printStackTrace(),
                        () -> Log.e("complete","YUP, has been completed")
                );
    }

}
