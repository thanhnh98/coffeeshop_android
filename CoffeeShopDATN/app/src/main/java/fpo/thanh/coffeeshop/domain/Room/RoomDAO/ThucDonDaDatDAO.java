package fpo.thanh.coffeeshop.domain.Room.RoomDAO;

import androidx.room.Dao;
import androidx.room.Insert;

import fpo.thanh.coffeeshop.domain.Room.RoomModel.ThucDonDaDat;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ThucDonDaDatDAO {
    @Insert(onConflict = REPLACE)
    void insertThucDonDaDat(ThucDonDaDat thucdon);
}
