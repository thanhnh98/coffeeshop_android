package fpo.thanh.coffeeshop.domain.Room.RoomDAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fpo.thanh.coffeeshop.domain.Room.RoomModel.DatMon;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface DatMonDAO {
    @Insert(onConflict = REPLACE)
    public void insertDatMon(DatMon datMon);
    @Query("SELECT * FROM DATMON WHERE maBan=:maBan")
    public List<DatMon> getDsDatMonByMaBan(String maBan);
}
