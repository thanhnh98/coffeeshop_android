package fpo.thanh.coffeeshop.presentation.presenter.impl;

import fpo.thanh.coffeeshop.domain.executor.Executor;
import fpo.thanh.coffeeshop.domain.executor.MainThread;
import fpo.thanh.coffeeshop.domain.executor.impl.ThreadExecutor;
import fpo.thanh.coffeeshop.domain.interactors.GetOrderInteractor;
import fpo.thanh.coffeeshop.domain.interactors.base.Interactor;
import fpo.thanh.coffeeshop.domain.interactors.impl.GetOrderInteractorImpl;
import fpo.thanh.coffeeshop.domain.repository.GetOrderRepository;
import fpo.thanh.coffeeshop.presentation.presenter.AbstractPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.GetOrderPresenter;
import fpo.thanh.coffeeshop.shareModel.GetOrderModel;
import fpo.thanh.coffeeshop.threading.MainThreadImpl;

public class GetOrderPresenterImpl extends AbstractPresenter implements GetOrderInteractor.Callback, GetOrderPresenter {
    private final View view;
    private final GetOrderRepository repository;

    public GetOrderPresenterImpl(Executor executor, MainThread mainThread, View view, GetOrderRepository repository) {
        super(executor, mainThread);
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void onFinish(GetOrderModel result) {
        view.showResultGetOrder(result);
    }

    @Override
    public void requestGetOrder(String id) {
        GetOrderInteractor interactor=new GetOrderInteractorImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),
                this,id,repository);
        interactor.execute();
    }
}
