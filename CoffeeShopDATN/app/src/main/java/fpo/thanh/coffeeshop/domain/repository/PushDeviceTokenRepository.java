package fpo.thanh.coffeeshop.domain.repository;

import fpo.thanh.coffeeshop.shareModel.PushTokenModel;

public interface PushDeviceTokenRepository {
    PushTokenModel pushTokenDevice(String token);
}
