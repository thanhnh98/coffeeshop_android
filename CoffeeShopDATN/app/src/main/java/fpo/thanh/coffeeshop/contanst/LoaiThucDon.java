package fpo.thanh.coffeeshop.contanst;

public enum LoaiThucDon {
    Coffee("Coffee"),
    Drink("Drink"),
    Other("Other"),
    Fast_Food("Fast Food"),
    Others("Other"),
    DaXay("Đá Xay"),
    Freeze("Freeze"),
    ALL("*");
    private String text;
    private LoaiThucDon(String text) {
        this.text = text;
    }
    public String getText() {
        return text;
    }
}
