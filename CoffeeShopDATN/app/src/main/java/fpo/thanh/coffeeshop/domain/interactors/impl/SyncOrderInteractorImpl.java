package fpo.thanh.coffeeshop.domain.interactors.impl;

import fpo.thanh.coffeeshop.domain.executor.Executor;
import fpo.thanh.coffeeshop.domain.executor.MainThread;
import fpo.thanh.coffeeshop.domain.interactors.SyncOrderInteractor;
import fpo.thanh.coffeeshop.domain.interactors.base.AbstractInteractor;
import fpo.thanh.coffeeshop.domain.repository.SyncOrderRepository;
import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;

public class SyncOrderInteractorImpl extends AbstractInteractor implements SyncOrderInteractor {
    private final Callback callback;
    private final SyncOrderRepository repository;
    private final int maHoaDon;
    private final String maHoaDonLocal;
    private final String maNhanVienOrder;
    private final String thoiGianLap;
    private final int maBan;
    private final String tenBan;
    private final String tokenDevice;
    private final Double tongTien;
    private final int giamGia;
    private final Double thanhTien;
    private final String dsMonJs;
    private final int trangThai;

    public SyncOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback callback,
                                   SyncOrderRepository repository,
                                   int maHoaDon, String maHoaDonLocal, String maNhanVienOrder, String thoiGianLap,
                                   int maBan, String tenBan, String tokenDevice, Double tongTien, int giamGia, Double thanhTien, String dsMonJs,int trangThai) {
        super(threadExecutor, mainThread);
        this.callback = callback;
        this.repository = repository;
        this.maHoaDon = maHoaDon;
        this.maHoaDonLocal = maHoaDonLocal;
        this.maNhanVienOrder = maNhanVienOrder;
        this.thoiGianLap = thoiGianLap;
        this.maBan = maBan;
        this.tenBan = tenBan;
        this.tokenDevice = tokenDevice;
        this.tongTien = tongTien;
        this.giamGia = giamGia;
        this.thanhTien = thanhTien;
        this.dsMonJs = dsMonJs;
        this.trangThai = trangThai;
    }

    @Override
    public void run() {
        final SyncOrderModel result=repository.syncOrder(maHoaDon,maHoaDonLocal,maNhanVienOrder,thoiGianLap,maBan,tenBan,tokenDevice,tongTien,giamGia,thanhTien,dsMonJs,trangThai);
        mMainThread.post(()->callback.onFinish(result));

    }
}
