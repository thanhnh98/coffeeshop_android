package fpo.thanh.coffeeshop.domain.repository.Impl;

import fpo.thanh.coffeeshop.domain.repository.GetOrderRepository;
import fpo.thanh.coffeeshop.shareModel.GetOrderModel;
import fpo.thanh.coffeeshop.storage.network.RestClient;
import fpo.thanh.coffeeshop.storage.service.GetOrderService;
import retrofit2.Response;

public class GetOrderRepositoryImpl implements GetOrderRepository {
    @Override
    public GetOrderModel getOrder(String id) {
        GetOrderService service= RestClient.createService(GetOrderService.class);
        try{
            Response<GetOrderModel> response=service.getOrderById(id).execute();
            if(response.isSuccessful()){
                return response.body();
            }
        }catch (Exception e){

        }
        return null;
    }
}
