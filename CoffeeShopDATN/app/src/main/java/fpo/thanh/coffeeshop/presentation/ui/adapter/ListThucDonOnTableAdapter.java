package fpo.thanh.coffeeshop.presentation.ui.adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.ThucDon;
import fpo.thanh.coffeeshop.presentation.ui.Dialog.EditThucDonDialog;
import fpo.thanh.coffeeshop.presentation.ui.fragment.ListThucDocFragment;
import fpo.thanh.coffeeshop.presentation.ui.fragment.ListThucDonOnTableFragment;

import static java.lang.Integer.min;

public class ListThucDonOnTableAdapter extends RecyclerView.Adapter<ListThucDonOnTableAdapter.ViewHolder> {

    private final Context context;
    private ListThucDonOnTableFragment fragment;
    private ListThucDocFragment listThucDocFragment;
    private final List<ThucDon> listData;
    private final List<Integer> listSoLuong;

    public ListThucDonOnTableAdapter(Context context, ListThucDonOnTableFragment fragment, ListThucDocFragment listThucDocFragment,List<ThucDon> listData, List<Integer> listSoLuong){

        this.context = context;
        this.fragment = fragment;
        this.listThucDocFragment = listThucDocFragment;
        this.listData = listData;
        this.listSoLuong = listSoLuong;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.layout_itemCart.setVisibility(View.VISIBLE);
            holder.tv_giaThucDonCart.setText(String.valueOf(listData.get(position).gia * listSoLuong.get(position)) + "$");
            holder.tv_nameThucDonCart.setText(listData.get(position).tenThucDon);
            holder.tv_soLuongCart.setText(String.valueOf(listSoLuong.get(position)) + "x");
            holder.layout_itemCart.setOnClickListener(v -> {
                EditThucDonDialog dialog = new EditThucDonDialog(context,this,position, listData.get(position).tenThucDon, listSoLuong.get(position),listData.get(position).gia);
                dialog.show(((AppCompatActivity) context).getSupportFragmentManager(), "Thanh");
                Log.e("TESt dialog", "TEST");
            });
        }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public int getItemCount() {
        return min(listData.size(),listSoLuong.size());
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_soLuongCart)
        TextView tv_soLuongCart;
        @BindView(R.id.tv_giaThucDonCart)
        TextView tv_giaThucDonCart;
        @BindView(R.id.tv_nameThucDonCart)
        TextView tv_nameThucDonCart;
        @BindView(R.id.layout_itemCart)
        ConstraintLayout layout_itemCart;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void capNhatSoLuong(int position,int value){
        if(value==0){
            listThucDocFragment.updateValueHashFromDonHang(listData.get(position).maThucDon,value);
            listSoLuong.remove(position);
            listData.remove(position);
        }
        else{
            listSoLuong.set(position,value);
            listThucDocFragment.updateValueHashFromDonHang(listData.get(position).maThucDon,value);
        }

        for(int i:listSoLuong) Log.e("i = ",""+i); //tys chuyeenr cai nay` qua fragment, o day no ko rs lai item
        fragment.updateTongGia(listData,listSoLuong);
        this.notifyDataSetChanged();
    }
}
