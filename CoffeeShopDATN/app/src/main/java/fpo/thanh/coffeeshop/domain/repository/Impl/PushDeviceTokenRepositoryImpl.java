package fpo.thanh.coffeeshop.domain.repository.Impl;

import fpo.thanh.coffeeshop.domain.repository.PushDeviceTokenRepository;
import fpo.thanh.coffeeshop.shareModel.PushTokenModel;
import fpo.thanh.coffeeshop.storage.network.RestClient;
import fpo.thanh.coffeeshop.storage.service.PushDeviceTokenService;
import retrofit2.Response;

public class PushDeviceTokenRepositoryImpl implements PushDeviceTokenRepository {

    @Override
    public PushTokenModel pushTokenDevice(String token) {
        PushDeviceTokenService service= RestClient.createService(PushDeviceTokenService.class);
        try{
            Response<PushTokenModel> response=service.pushToken(token).execute();
            if(response.isSuccessful()){
                return response.body();
            }
        }
        catch (Exception e){

        }
        return null;
    }

}
