package fpo.thanh.coffeeshop.presentation.ui.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.contanst.CacheKey;
import fpo.thanh.coffeeshop.contanst.STATUS_ORDER;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.domain.executor.impl.ThreadExecutor;
import fpo.thanh.coffeeshop.domain.repository.Impl.SyncOrderRepositoryImpl;
import fpo.thanh.coffeeshop.presentation.presenter.SyncOrderPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.impl.SyncOrderPresenterImpl;
import fpo.thanh.coffeeshop.presentation.ui.activity.MainActivity;
import fpo.thanh.coffeeshop.presentation.ui.activity.SupportActivity;
import fpo.thanh.coffeeshop.presentation.ui.fragment.DetailSingleOrderFragment;
import fpo.thanh.coffeeshop.shareModel.PostChiTietModel;
import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;
import fpo.thanh.coffeeshop.storage.savePreference.CacheClient;
import fpo.thanh.coffeeshop.threading.MainThreadImpl;

public class ListHoaDonPedingAdapter extends RecyclerView.Adapter<ListHoaDonPedingAdapter.ViewHolder> implements SyncOrderPresenter.View {
    private final Context context;
    private final List<HoaDon> listData;
    private HoaDon hoaDon;
    private HoaDon hoaDonOnclick;
    String json="";
    private MainActivity activity;
    private SyncOrderPresenter presenter;
    private ViewBinderHelper viewBinderHelper;

    public ListHoaDonPedingAdapter(Context context, List<HoaDon> listData){
        viewBinderHelper=new ViewBinderHelper();
        viewBinderHelper.setOpenOnlyOne(true);
        this.context = context;
        this.listData = listData;
        activity=(MainActivity)context;
        presenter=new SyncOrderPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),this,new SyncOrderRepositoryImpl());
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listhoadon,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        hoaDon=listData.get(position);
        holder.tv_maHoaDon.setText(hoaDon.getMaHoaDonLocal());
        holder.tv_giaSanPhamHoaDon.setText(String.valueOf(hoaDon.getThanhTien()));
        holder.tv_tenBanHoaDon.setText(String.valueOf(hoaDon.getTenBan()));
        holder.tv_thoiGianHoaDon.setText(hoaDon.getThoiGianLap());
        holder.tv_trangThaiHoaDon.setText("Đợi xử lý");
        setStatusStyle(holder.tv_trangThaiHoaDon,hoaDon.getTrangThai());
        viewBinderHelper.bind(holder.swipeHoaDonPending,listData.get(position).maHoaDonLocal);
        //Log.e(hoaDon.getTenBan(),hoaDon.getTrangThai()+"");
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public void onShowResultSyncOrder(SyncOrderModel result) {
        try {
            if(result.getExitCode()==1){
                Toast.makeText(context, "Thanhf coong", Toast.LENGTH_SHORT).show();
            }
            else Log.e("result sync",result.getExitCode()+"");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.tv_tenBanHoaDon)
        TextView tv_tenBanHoaDon;
        @BindView(R.id.tv_thoiGianHoaDon)
        TextView tv_thoiGianHoaDon;
        @BindView(R.id.tv_maHoaDon)
        TextView tv_maHoaDon;
        @BindView(R.id.tv_trangThaiHoaDon)
        TextView tv_trangThaiHoaDon;
        @BindView(R.id.tv_giaSanPhamHoaDon)
        TextView tv_giaSanPhamHoaDon;
        @BindView(R.id.layout_listhoadon)
        ConstraintLayout layout_listhoadon;
        @BindView(R.id.tv_huyHoaDon)
        TextView tv_huyHoaDon;
        @BindView(R.id.swipeHoaDonPending)
        SwipeRevealLayout swipeHoaDonPending;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            layout_listhoadon.setOnClickListener(this);
            tv_huyHoaDon.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.layout_listhoadon:
                    hoaDonOnclick=listData.get(getAdapterPosition());
                    Log.e("maHoaDonLocal",hoaDonOnclick.getMaHoaDonLocal());
                    String tokenFirebase=CacheClient.getCache(context,"tokenfirebase");
                    List<DsMon> listDsMon=activity.mDB.dsMonDAO().getDsMonByMaHoadonLocal(hoaDonOnclick.getMaHoaDonLocal());
                    Log.e("size ban ddaauf moi get",listDsMon+"");
//                    presenter.requestSyncOrder(hoaDonOnclick.getMaHoaDon(),hoaDon.getMaHoaDonLocal(),"",hoaDonOnclick.getThoiGianLap(),
//                            hoaDonOnclick.getMaBan(),hoaDonOnclick.getTenBan(),tokenFirebase,hoaDonOnclick.tongTien,0,
//                            hoaDonOnclick.thanhTien,convertJsonFromListDsMon(listDsMon),1);

//                    Log.e("maHoaDon",String.valueOf(hoaDonOnclick.getMaHoaDon()));
//                    Log.e("maHoaDonLocal",String.valueOf(hoaDonOnclick.getMaHoaDonLocal()));
//                    Log.e("maNhanVien","khoong guiwr");
//                    Log.e("thoiGianLap",String.valueOf(hoaDonOnclick.getThoiGianLap()));
//                    Log.e("maBan",String.valueOf(hoaDonOnclick.getMaBan()));
//                    Log.e("tokenFirebase",tokenFirebase);
//                    Log.e("tongTien",""+hoaDonOnclick.getTongTien());
//                    Log.e("giamGia","0");
//                    Log.e("thanhTien",""+hoaDonOnclick.getThanhTien());
//                    Log.e("listDsMon",convertJsonFromListDsMon(listDsMon));

//                    ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction()
//                            .addToBackStack("holder")
//                            .replace(R.id.layout_holder, DetailSingleOrderFragment.getInstance(listData.get(getAdapterPosition()).getMaHoaDonLocal()))
//                            .commit();
                    Intent intent=new Intent(context, SupportActivity.class);
                   // Log.e("maHoadonLocal","ở adapter: "+listData.get(getAdapterPosition()).getMaHoaDonLocal());
                    intent.putExtra("maHoaDonLocal",listData.get(getAdapterPosition()).getMaHoaDonLocal());
                    intent.putExtra("requestCode",1);
                    context.startActivity(intent);


                    break;
                case R.id.tv_huyHoaDon:
                    new AlertDialog.Builder(context).setMessage("Chắc chắn hủy hóa đơn này?")
                            .setTitle("Xác nhận hủy")
                            .setPositiveButton("Hủy đơn",(dialog,which)->{
                                HoaDon hoaDon=listData.get(getAdapterPosition());
                                hoaDon.setHasSync(1);
                                hoaDon.setTrangThai(3);
                                activity.mDB.hoaDonDAO().insertHoaDon(hoaDon);
                                listData.remove(getAdapterPosition());
                                notifyItemRemoved(getAdapterPosition());
                            }).setNegativeButton("Đóng",((dialog, which) -> dialog.dismiss())).show();

                    break;
                    default:break;
            }
        }
    }
    private String convertJsonFromListDsMon(List<DsMon> dsMons){
        Log.e("size",dsMons.size()+"");

        json="[";
        Gson gson=new Gson();
        for(DsMon dsMon:dsMons){
            Log.e("tesst convertjson", dsMon.getMaChiTietLocal());
            json+=gson.toJson(new PostChiTietModel(0,dsMon.getMaHoaDon(),dsMon.getMaHoaDonLocal(),dsMon.getMaThucDon(),
                    dsMon.getSoLuong(),dsMon.getDonGia(),dsMon.getMaChiTietLocal(),dsMon.getTrangThai()));
            json+=",";
        }
        StringBuilder resultJson=new StringBuilder(json);
        if(resultJson.length()>1)
            resultJson.setCharAt(json.length()-1,']');
        else return json+']';
        Log.e("Json peding",resultJson.toString());
        return resultJson.toString();
    }
    private void setStatusStyle(TextView tv,int s){
        switch (s){
            case 1: { //new (hóa đơn mới)
                //Log.e("??","sao ko dodoir mauf ta");
                tv.setTextColor(context.getResources().getColor(R.color.status_pending));
                break;
            }
            case 2: { //update (đã cập nhật)
                tv.setTextColor(context.getResources().getColor(R.color.status_capnhat));
                break;

            }
            case 3: { //cancel (đã hủy)
                tv.setTextColor(context.getResources().getColor(R.color.status_huy));
                break;

            }
            case 4: { //change ignore
                tv.setTextColor(context.getResources().getColor(R.color.status_pending));
                break;
            }
            case 5: { //merge
                tv.setTextColor(context.getResources().getColor(R.color.status_pending));
                break;
            }
            case 6: { //cooking (đang chế biến)
                tv.setTextColor(context.getResources().getColor(R.color.status_cooking));
                break;
            }
            case 7: { //cooked (đã trả món)
                tv.setTextColor(context.getResources().getColor(R.color.status_cooked));
                break;
            }
            case 8: { //complete (đã thanh toán)
                tv.setTextColor(context.getResources().getColor(R.color.status_completed));
                break;
            }
        }
    }
}
