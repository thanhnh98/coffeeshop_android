package fpo.thanh.coffeeshop.presentation.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.contanst.CacheKey;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.ThucDon;
import fpo.thanh.coffeeshop.presentation.ui.fragment.BottomFragmentSheet;
import fpo.thanh.coffeeshop.presentation.ui.fragment.ListThucDocFragment;

public class ListThucDonAdapter extends RecyclerView.Adapter<ListThucDonAdapter.ViewHolder> {
    private final Context context;
    private ListThucDocFragment fragment;
    private final int maBan;
    private final int maTang;
    private final List<ThucDon> listData;

    public ListThucDonAdapter(Context context, ListThucDocFragment fragment, int maBan, int maTang, List<ThucDon> listData){

        this.context = context;
        this.fragment = fragment;
        this.maBan = maBan;
        this.maTang = maTang;
        this.listData = listData;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listthucdon,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_nameThucDon.setText(listData.get(position).tenThucDon);
        holder.tv_priceThucDon.setText(String.valueOf(listData.get(position).gia)+"$");
        holder.tv_loaiThucDon.setText(listData.get(position).tenLoai);
        Picasso.get().load(CacheKey.RootImage+listData.get(position).hinhAnh).placeholder(R.drawable.holdercoffee).resize(200,200).onlyScaleDown()
                .into(holder.img_thucdon);
        //Log.e("Hình ảnh",CacheKey.RootImage+listData.get(position).hinhAnh);
        holder.layout_itemThucDon.setOnClickListener(v->{
//            View view = ((AppCompatActivity)context).getLayoutInflater().inflate(R.layout.bottomsheet_detailproduct, null);
//
//            BottomSheetDialog dialog = new BottomSheetDialog(context);
//            dialog.setContentView(view);
//            dialog.show();
            BottomFragmentSheet bottomSheetFragment = BottomFragmentSheet.getInstance(fragment,listData.get(position).maThucDon,listData.get(position).tenThucDon,listData.get(position).gia,listData.get(position).moTa,maBan,maTang);
            bottomSheetFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), bottomSheetFragment.getTag());
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_priceThucDon)
        TextView tv_priceThucDon;
        @BindView(R.id.tv_nameThucdon)
        TextView tv_nameThucDon;
        @BindView(R.id.img_thucdon)
        ImageView img_thucdon;
        @BindView(R.id.tv_loaiThucDon)
        TextView tv_loaiThucDon;
        @BindView(R.id.layout_itemThucDon)
        ConstraintLayout layout_itemThucDon;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
