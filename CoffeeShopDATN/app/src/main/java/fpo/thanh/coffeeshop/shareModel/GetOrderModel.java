package fpo.thanh.coffeeshop.shareModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;

public class GetOrderModel {
    @SerializedName("exit_code")
    @Expose
    private Integer exitCode;
    @SerializedName("data")
    @Expose
    private HoaDonModel data;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getExitCode() {
        return exitCode;
    }

    public void setExitCode(Integer exitCode) {
        this.exitCode = exitCode;
    }

    public HoaDonModel getData() {
        return data;
    }

    public void setData(HoaDonModel data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
