package fpo.thanh.coffeeshop.domain.Room.RoomModel;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class NotificationModel {
    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String getTimeGenerate() {
        return timeGenerate;
    }

    public void setTimeGenerate(String timeGenerate) {
        this.timeGenerate = timeGenerate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getHasRead() {
        return hasRead;
    }

    public void setHasRead(Integer hasRead) {
        this.hasRead = hasRead;
    }

    @PrimaryKey (autoGenerate = true)
    public Integer _id;
    @ColumnInfo (name = "timeGenerate")
    public String timeGenerate;
    @ColumnInfo (name = "content")
    public String content;
    @ColumnInfo (name = "hasRead")
    public Integer hasRead;
}
