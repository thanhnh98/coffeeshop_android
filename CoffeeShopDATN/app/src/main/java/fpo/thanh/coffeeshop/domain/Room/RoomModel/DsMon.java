package fpo.thanh.coffeeshop.domain.Room.RoomModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class DsMon {
    @NonNull
    @PrimaryKey(autoGenerate = false)
    public String maChiTietLocal="";
    @ColumnInfo(name = "tenThucDon")
    public String tenThucDon;
    @ColumnInfo(name = "giaMon")
    public Double giaMon;
    @ColumnInfo(name="maChiTiet")
    public Integer maChiTiet;
    @ColumnInfo(name = "maHoaDon")
    public Integer maHoaDon;
    @ColumnInfo(name="maThucDon")
    public Integer maThucDon;
    @ColumnInfo(name = "soLuong")
    public Integer soLuong;
    @ColumnInfo(name = "donGia")
    public Double donGia;
    @ColumnInfo(name = "trangThai")
    public Integer trangThai;

    public String getMaHoaDonLocal() {
        return maHoaDonLocal;
    }

    public void setMaHoaDonLocal(String maHoaDonLocal) {
        this.maHoaDonLocal = maHoaDonLocal;
    }

    @ColumnInfo(name = "maHoaDonLocal")
    public String maHoaDonLocal;


    public String getTenThucDon() {
        return tenThucDon;
    }

    public void setTenThucDon(String tenThucDon) {
        this.tenThucDon = tenThucDon;
    }

    public Double getGiaMon() {
        return giaMon;
    }

    public void setGiaMon(Double giaMon) {
        this.giaMon = giaMon;
    }

    public Integer getMaChiTiet() {
        return maChiTiet;
    }

    public void setMaChiTiet(Integer maChiTiet) {
        this.maChiTiet = maChiTiet;
    }

    public Integer getMaHoaDon() {
        return maHoaDon;
    }

    public void setMaHoaDon(Integer maHoaDon) {
        this.maHoaDon = maHoaDon;
    }

    public Integer getMaThucDon() {
        return maThucDon;
    }

    public void setMaThucDon(Integer maThucDon) {
        this.maThucDon = maThucDon;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public Double getDonGia() {
        return donGia;
    }

    public void setDonGia(Double donGia) {
        this.donGia = donGia;
    }

    public String getMaChiTietLocal() {
        return maChiTietLocal;
    }

    public void setMaChiTietLocal(String maChiTietLocal) {
        this.maChiTietLocal = maChiTietLocal;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

}
