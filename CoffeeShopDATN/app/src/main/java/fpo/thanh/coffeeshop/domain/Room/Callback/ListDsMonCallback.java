package fpo.thanh.coffeeshop.domain.Room.Callback;

import android.content.Context;

import java.util.List;

import fpo.thanh.coffeeshop.domain.Room.AppDatabase;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;
import fpo.thanh.coffeeshop.domain.executor.MainThread;

public class ListDsMonCallback implements ListDsMonCallbackInterface {
    private final Context context;
    private AppDatabase mDb;
    private final View view;
    private final MainThread mainThread;

    public ListDsMonCallback(Context context, AppDatabase mDb, View view, MainThread mainThread){

        this.context = context;
        this.mDb = mDb;
        this.view = view;
        this.mainThread = mainThread;
    }
    @Override
    public void requestDsMon(String maHoaDonLocal) {
        mainThread.post(()->{
            List<DsMon> result=mDb.dsMonDAO().getDsMonByMaHoadonLocal(maHoaDonLocal);
            view.resultDsMon(result);
        });
    }
}
