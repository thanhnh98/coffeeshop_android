package fpo.thanh.coffeeshop.domain.Room.RoomModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DonHang {
    @NonNull
    @PrimaryKey(autoGenerate = false)
    public Integer maDonHang;
    @ColumnInfo(name = "thoiGianLap")
    public String thoiGianLap;
    @ColumnInfo(name = "maNhanVien")
    public Integer maNhanVien;
    @ColumnInfo(name = "maBan")
    public Integer maBan;
    @ColumnInfo(name = "tongTien")
    public Integer tongTien;
    @ColumnInfo(name = "trangThai")
    public String trangThai;
    @ColumnInfo(name = "danhSachSanPham")
    public String danhSachSanPham;
}
