package fpo.thanh.coffeeshop.storage.service;

import fpo.thanh.coffeeshop.shareModel.GetOrderModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface GetOrderService {
    @POST("/api/getOrderFromServer")
    @FormUrlEncoded
    Call<GetOrderModel> getOrderById(@Field("id")String id);

}
