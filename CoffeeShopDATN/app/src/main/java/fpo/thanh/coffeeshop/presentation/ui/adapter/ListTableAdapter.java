package fpo.thanh.coffeeshop.presentation.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.github.marlonlom.utilities.timeago.TimeAgoMessages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.contanst.CacheKey;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.BanAn;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.presentation.ui.activity.SupportActivity;
import fpo.thanh.coffeeshop.presentation.ui.fragment.DetailSingleOrderFragment;
import fpo.thanh.coffeeshop.presentation.ui.listeners.RefreshListTableCallback;

public class ListTableAdapter extends RecyclerView.Adapter<ListTableAdapter.ViewHolder> {

    private final Context context;
    private final List<BanAn> listData;
    Locale LocaleBylanguageTag;
    TimeAgoMessages messages;
    long timeInMillis;
    HoaDon hoaDon;
    private List<HoaDon> listHoaDon;
    private RefreshListTableCallback callback;

    public ListTableAdapter(Context context, List<BanAn> listData, List<HoaDon> listHoaDon, RefreshListTableCallback callback){
        this.listHoaDon = listHoaDon;
        this.callback = callback;
        LocaleBylanguageTag = Locale.forLanguageTag("en");
        messages = new TimeAgoMessages.Builder().withLocale(LocaleBylanguageTag).build();
        timeInMillis = System.currentTimeMillis()-360000000;
       // Log.e("milis",""+timeInMillis);
        this.context = context;
        this.listData = listData;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listtable,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if(checkBanDangSuDung(listData.get(position).tenBan)!="null"){
            String myDate = hoaDon.getThoiGianLap();
            //Log.e(listData.get(position).maBan+"",myDate);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            try {
                date = sdf.parse(myDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long millis = date.getTime();
            String text= TimeAgo.using(millis,messages);
           // Log.e(listData.get(position).maBan+"",listData.get(position).tenBan);
           // holder.tv_timeAgo.setText(text);
            holder.tv_timeAgo.setText(getRelationTime(millis));
            holder.tv_nhanvienAccount.setText(""+hoaDon.getTenNhanVienOrder());
            holder.tv_timeAgo.setVisibility(View.VISIBLE);
            holder.tv_nhanvienAccount.setVisibility(View.VISIBLE);
            if (hoaDon.getTrangThai()==7){
                setBackground(holder.cardView,holder.tv1,holder.tv2,holder.tv3,holder.tv4,2);
            }else
                setBackground(holder.cardView,holder.tv1,holder.tv2,holder.tv3,holder.tv4,1);
        }else{
            setBackground(holder.cardView,holder.tv1,holder.tv2,holder.tv3,holder.tv4,0);
            holder.tv_timeAgo.setText("");
            holder.tv_nhanvienAccount.setText("");
            holder.tv_timeAgo.setVisibility(View.INVISIBLE);
            holder.tv_nhanvienAccount.setVisibility(View.INVISIBLE);
        }


        //holder.tv_timeAgo.setText(text);
        holder.tv_nameBanAn.setText(listData.get(position).tenBan);
        //holder.tv_nhanvienAccount.setText("Nhân viên " +position);
        holder.cardView.setOnClickListener(v->{
            String maHoaDonLocal=checkBanDangSuDung(listData.get(position).tenBan);
            if(maHoaDonLocal=="null") { //Nếu bàn chưa đặt
                Intent intent = new Intent(context, SupportActivity.class);
                intent.putExtra("tenBan", listData.get(position).tenBan);
                intent.putExtra("maTang", listData.get(position).maTang);
                intent.putExtra("maBan", listData.get(position).maBan);
                intent.putExtra("requestCode", 0);
                intent.putExtra("addSign",0);
                //context.startActivity(intent);
                context.startActivity(intent);
            }else{ //Nếu bàn đặt rồi
                Intent intent= new Intent(context,SupportActivity.class);
                intent.putExtra("maHoaDonLocal",maHoaDonLocal);
                intent.putExtra("requestCode",1);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_timeAgo)
        TextView tv_timeAgo;
        @BindView(R.id.tv_nameBanAn)
        TextView tv_nameBanAn;
        @BindView(R.id.tv_nhanvienAccount)
        TextView tv_nhanvienAccount;
        @BindView(R.id.layoutTable)
        ConstraintLayout cardView;
        @BindView(R.id.tv1)
        TextView tv1;
        @BindView(R.id.tv2)
        TextView tv2;
        @BindView(R.id.tv3)
        TextView tv3;
        @BindView(R.id.tv4)
        TextView tv4;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    private String checkBanDangSuDung(String tenBan){
        for(int i=0;i<listHoaDon.size();++i){
            //Log.e("check nhẹ trang thai",""+listHoaDon.get(i).getTrangThai());
            if((listHoaDon.get(i).getTrangThai()==1||listHoaDon.get(i).getTrangThai()==2||listHoaDon.get(i).getTrangThai()==6||listHoaDon.get(i).getTrangThai()==7)&&tenBan.equals(listHoaDon.get(i).getTenBan())){
                hoaDon=listHoaDon.get(i);
                return hoaDon.getMaHoaDonLocal();
            }
        }
        return "null";
    }
    private void setBackground(ConstraintLayout tv,TextView tv1,TextView tv2,TextView tv3,TextView tv4,int activeCode){
        if(activeCode==0){
            tv.setBackground(context.getResources().getDrawable(R.drawable.background_table_non_active,null));
            tv1.setBackground(context.getResources().getDrawable(R.drawable.background_table_non_active,null));
            tv2.setBackground(context.getResources().getDrawable(R.drawable.background_table_non_active,null));
            tv3.setBackground(context.getResources().getDrawable(R.drawable.background_table_non_active,null));
            tv4.setBackground(context.getResources().getDrawable(R.drawable.background_table_non_active,null));
        }else if (activeCode==1){
            tv.setBackground(context.getResources().getDrawable(R.drawable.background_table_active,null));
            tv1.setBackground(context.getResources().getDrawable(R.drawable.background_table_active,null));
            tv2.setBackground(context.getResources().getDrawable(R.drawable.background_table_active,null));
            tv3.setBackground(context.getResources().getDrawable(R.drawable.background_table_active,null));
            tv4.setBackground(context.getResources().getDrawable(R.drawable.background_table_active,null));
        }else if(activeCode==2){
            //đã trả món
            tv.setBackground(context.getResources().getDrawable(R.drawable.background_table_datramon,null));
            tv1.setBackground(context.getResources().getDrawable(R.drawable.background_table_datramon,null));
            tv2.setBackground(context.getResources().getDrawable(R.drawable.background_table_datramon,null));
            tv3.setBackground(context.getResources().getDrawable(R.drawable.background_table_datramon,null));
            tv4.setBackground(context.getResources().getDrawable(R.drawable.background_table_datramon,null));
        }
    }

    public static final long AVERAGE_MONTH_IN_MILLIS = DateUtils.DAY_IN_MILLIS * 30;

    private String getRelationTime(long time) {
        final long now = new Date().getTime();
        final long delta = now - time;
        long resolution;
        if (delta <= DateUtils.MINUTE_IN_MILLIS) {
            resolution = DateUtils.SECOND_IN_MILLIS;
        } else if (delta <= DateUtils.HOUR_IN_MILLIS) {
            resolution = DateUtils.MINUTE_IN_MILLIS;
        } else if (delta <= DateUtils.DAY_IN_MILLIS) {
            resolution = DateUtils.HOUR_IN_MILLIS;
        } else if (delta <= DateUtils.WEEK_IN_MILLIS) {
            resolution = DateUtils.DAY_IN_MILLIS;
        } else if (delta <= AVERAGE_MONTH_IN_MILLIS) {
            return Integer.toString((int) (delta / DateUtils.WEEK_IN_MILLIS)) + " ngày truóc";
        } else if (delta <= DateUtils.YEAR_IN_MILLIS) {
            return Integer.toString((int) (delta / AVERAGE_MONTH_IN_MILLIS)) + " tháng truóc";
        } else {
            return Integer.toString((int) (delta / DateUtils.YEAR_IN_MILLIS)) + " năm trước";
        }
        return DateUtils.getRelativeTimeSpanString(time, now, resolution).toString();
    }

}
