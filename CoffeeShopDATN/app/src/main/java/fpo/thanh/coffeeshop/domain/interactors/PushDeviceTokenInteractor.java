package fpo.thanh.coffeeshop.domain.interactors;

import fpo.thanh.coffeeshop.domain.interactors.base.Interactor;
import fpo.thanh.coffeeshop.shareModel.PushTokenModel;

public interface PushDeviceTokenInteractor extends Interactor {
    interface Callback{
        void onFinish(PushTokenModel result);
    }
}
