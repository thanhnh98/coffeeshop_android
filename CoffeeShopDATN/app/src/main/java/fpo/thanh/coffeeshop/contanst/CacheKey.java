package fpo.thanh.coffeeshop.contanst;

public class CacheKey {
    public static String TOKEN = "token";
    public static String RootImage=API.serverName+"/uploads/product_small/";
    public static String Coffee= "Coffee";
    public static String Drink="Drink";
    public static String Other="Other";
    public static String  Fast_Food="Fast Food";
    public static String Others="Other";
    public static String DaXay="Đá Xay";
    public static String Freeze="Freeze";
    public static int DELAY_TIME=7;
    public static String getTenTang(int maTang){
        switch (maTang){
            case 5:
                return "Tầng trệt";
            case 6:
                return "Tầng 01";
            case 7:
                return "Tầng 02";
            case 8:
                return "Sân vườn";
            case 9:
                return "Ban công";
            case 10:
                return "Tất cả";
                default:
                    return "";
        }
    }
}
