package fpo.thanh.coffeeshop.domain.interactors;

import fpo.thanh.coffeeshop.domain.interactors.base.Interactor;
import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;

public interface SyncOrderInteractor extends Interactor {
    interface Callback{
        void onFinish(SyncOrderModel result);
    }
}
