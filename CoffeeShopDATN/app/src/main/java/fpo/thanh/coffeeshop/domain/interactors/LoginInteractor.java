package fpo.thanh.coffeeshop.domain.interactors;

import fpo.thanh.coffeeshop.domain.interactors.base.Interactor;
import fpo.thanh.coffeeshop.shareModel.LoginModel;

public interface LoginInteractor extends Interactor {
    interface Callback{
        void onFinish(LoginModel result);
    }
}
