package fpo.thanh.coffeeshop.storage.service;

import fpo.thanh.coffeeshop.shareModel.LoginModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginService {
    @POST("/api/login")
    @FormUrlEncoded
    Call<LoginModel> login(@Field("username") String username, @Field("password") String password);
}
