package fpo.thanh.coffeeshop.storage.service;

import fpo.thanh.coffeeshop.shareModel.PushTokenModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PushDeviceTokenService {
    @FormUrlEncoded
    @POST("/api/pushToken")
    Call<PushTokenModel> pushToken(@Field("token")String token);
}