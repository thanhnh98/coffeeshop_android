package fpo.thanh.coffeeshop.presentation.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.contanst.CacheKey;
import fpo.thanh.coffeeshop.domain.Room.AppDatabase;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.domain.executor.impl.ThreadExecutor;
import fpo.thanh.coffeeshop.domain.repository.Impl.GetAllDataRepositoryImpl;
import fpo.thanh.coffeeshop.domain.repository.Impl.SyncOrderRepositoryImpl;
import fpo.thanh.coffeeshop.presentation.presenter.GetAllDataPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.SyncOrderPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.TestPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.impl.GetAllDataPresenterImpl;
import fpo.thanh.coffeeshop.presentation.presenter.impl.SyncOrderPresenterImpl;
import fpo.thanh.coffeeshop.presentation.presenter.impl.TestPresenterImpl;
import fpo.thanh.coffeeshop.presentation.ui.adapter.TabLayoutAdapter;
import fpo.thanh.coffeeshop.presentation.ui.fragment.HomeFragment;
import fpo.thanh.coffeeshop.presentation.ui.fragment.ListBookedFragment;
import fpo.thanh.coffeeshop.presentation.ui.fragment.ListBookingFragment;
import fpo.thanh.coffeeshop.presentation.ui.fragment.ListTableFragment;
import fpo.thanh.coffeeshop.shareModel.PostChiTietModel;
import fpo.thanh.coffeeshop.shareModel.ResultModel;
import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;
import fpo.thanh.coffeeshop.shareModel.TestModel;
import fpo.thanh.coffeeshop.storage.TestRepositoryImpl;
import fpo.thanh.coffeeshop.storage.savePreference.CacheClient;
import fpo.thanh.coffeeshop.threading.MainThreadImpl;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements SyncOrderPresenter.View {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edt_searchProduct)
    EditText edt_searchProduct;
    @BindView(R.id.tv_brand)
    TextView tv_brand;
    public AppDatabase mDB;
    SyncOrderPresenter presenter;
    private static int checkSync=0;
    List<HoaDon> listHoaDonPending=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setupPager(viewPager,tabLayout);
        mDB=AppDatabase.getDatabase(this);
        presenter=new SyncOrderPresenterImpl(ThreadExecutor.getInstance(),MainThreadImpl.getInstance(),this,new SyncOrderRepositoryImpl());
        edt_searchProduct.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)  hideKeyboardFrom(MainActivity.this,v);

            }
        });
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Observable.just(1)
                //.debounce(3000,TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .repeatWhen ((completed)->completed.delay(CacheKey.DELAY_TIME,TimeUnit.SECONDS))
                .subscribe(
                        next -> {
                            try {
                                listHoaDonPending = mDB.hoaDonDAO().getHoaDonLocal();
                                Log.e("5s trooi qua", "" + listHoaDonPending.size());

                                if (checkSync == 0 && listHoaDonPending.size() > 0) {
                                    Log.e("5s trooi qua trong iff", "" + listHoaDonPending.size());
                                    HoaDon hoaDon = listHoaDonPending.get(0);
                                    Log.e("json cua hoadon",new Gson().toJson(hoaDon).toString());
                                    presenter.requestSyncOrder(hoaDon.getMaHoaDon(),hoaDon.getMaHoaDonLocal(),CacheClient.getCache(this,"maNhanVien"),hoaDon.getThoiGianLap(),
                                            hoaDon.getMaBan(),hoaDon.getTenBan(),CacheClient.getCache(this,"tokenfirebase"),hoaDon.tongTien,0,
                                            hoaDon.thanhTien,convertJsonFromListDsMon(mDB.dsMonDAO().getDsMonByMaHoadonLocal(hoaDon.getMaHoaDonLocal())),hoaDon.getTrangThai());
                                    Log.e("firebae token",CacheClient.getCache(this,"tokenfirebase"));
                                    checkSync = 1;
                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();
                                Log.e("Looix","nhung ko sao");
                                checkSync=0;
                            }
                                //Log.e(listHoaDonPending.get(1).getMaHoaDonLocal(),convertJsonFromListDsMon(mDB.dsMonDAO().getDsMonByMaHoadonLocal(listHoaDonPending.get(1).getMaHoaDonLocal())));
                               // if()

                            },
                        error -> error.printStackTrace(),
                        () -> Log.e("complete","YUP, has been completed")
                );

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return  true;
    }

    void setupPager(ViewPager viewPager, TabLayout tabLayout) {
        TabLayoutAdapter adapter = new TabLayoutAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "Home");
        adapter.addFragment(new ListTableFragment(), "Tables");
        adapter.addFragment(new ListBookingFragment(), "Booking");
        adapter.addFragment(new ListBookedFragment(), "Booked");
        viewPager.setAdapter(adapter);



        tabLayout.setupWithViewPager(viewPager, true);
        int[] tabIcons = {
                R.drawable.ic_news,
                R.drawable.ic_coffeecup,
                R.drawable.ic_coffeewaiting,
                R.drawable.ic_complete,
        };
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        tv_brand.setVisibility(View.VISIBLE);
//                        Animation a = AnimationUtils.loadAnimation(MainActivity.this, R.anim.anim_textview);
//                        a.reset();
//                        tv_brand.clearAnimation();
//                        tv_brand.startAnimation(a);

                        break;
                    case 1:
                        tv_brand.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        tv_brand.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        tv_brand.setVisibility(View.VISIBLE);
                        break;
                        default: break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.logout){
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setMessage("Bạn muốn thoát tài khoản?")
                    .setPositiveButton("Thoát", ((dialog, which) -> {
                        startActivity(new Intent(MainActivity.this,LoginActivity.class));
                        CacheClient.clearCache(this);
                        this.finish();
                    }))
                    .setNegativeButton("Đóng",(dialog,which)->{
                        dialog.dismiss();
                    }).show();
        }
        return true;
    }

    public void moveToPositionViewpager(int position){
        viewPager.setCurrentItem(position,true);
    }
    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    int d=0;
    @Override
    public void onBackPressed() {
           /* if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }*/


        if (viewPager.getCurrentItem()!=0) {
            viewPager.setCurrentItem(0,true);
        } else {


            if (getFragmentManager().getBackStackEntryCount() > 1) {
                getFragmentManager().popBackStack();
                d=0;
            } else {//nhan lan nua de thoat
                d++;
            }

            if(d==1){
                Toast.makeText(this, "Nhấn lần nữa để thoát!", Toast.LENGTH_SHORT).show();
                CountDownTimer timer=new CountDownTimer(2000,1000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        d=0;
                    }
                }.start();
            }
            if(d==2){
                    /*Intent i = new Intent(MainActivity.this,AuthorizeActivity.class);
                    startActivity(i);*/
                finish();
                // super.onBackPressed();
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Log.e("Close ActivityMain DB","close");
        //mDB.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //mDB=AppDatabase.getDatabase(this);
    }
    ListBookingFragment.RefreshBookingList refresh=new ListBookingFragment.RefreshBookingList();
    @Override
    public void onShowResultSyncOrder(SyncOrderModel result) {
        try{
            if(result.getExitCode()==1){
                HoaDon hoaDon=mDB.hoaDonDAO().getHoaDonByMaHoaDonLocal(result.getData().getMaHoaDonLocal());
                hoaDon.setHasSync(1);
                mDB.hoaDonDAO().insertHoaDon(hoaDon);
                listHoaDonPending.remove(0);
                checkSync=0;
                refresh.execute("Excuting");
            }else {
                Log.e("Sync Fail","sync fail");
                checkSync=0;
            }
        }catch (Exception e){
            checkSync=0;
            e.printStackTrace();
        }
    }
    private String convertJsonFromListDsMon(List<DsMon> dsMons){
        Log.e("size",dsMons.size()+"");
        String json;

        json="[";
        Gson gson=new Gson();
        for(DsMon dsMon:dsMons){
            Log.e("tesst convertjson", dsMon.getMaChiTietLocal());
            json+=gson.toJson(new PostChiTietModel(0,dsMon.getMaHoaDon(),dsMon.getMaHoaDonLocal(),dsMon.getMaThucDon(),
                    dsMon.getSoLuong(),dsMon.getDonGia(),dsMon.getMaChiTietLocal(),dsMon.getTrangThai()));
            json+=",";
        }
        StringBuilder resultJson=new StringBuilder(json);
        if(resultJson.length()>1)
            resultJson.setCharAt(json.length()-1,']');
        else return json+']';
        Log.e("Json peding",resultJson.toString());
        return resultJson.toString();
    }
}
