package fpo.thanh.coffeeshop.domain.repository.Impl;

import java.io.IOException;

import fpo.thanh.coffeeshop.domain.repository.LoginRepository;
import fpo.thanh.coffeeshop.shareModel.LoginModel;
import fpo.thanh.coffeeshop.storage.network.RestClient;
import fpo.thanh.coffeeshop.storage.service.LoginService;
import retrofit2.Response;

public class LoginRepositoryImpl implements LoginRepository {
    @Override
    public LoginModel login(String usernamespace, String password) {
        LoginService service= RestClient.createService(LoginService.class);
        try {
            Response<LoginModel> response=service.login(usernamespace,password).execute();
            if(response.isSuccessful())
                return response.body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new LoginModel();
    }
}
