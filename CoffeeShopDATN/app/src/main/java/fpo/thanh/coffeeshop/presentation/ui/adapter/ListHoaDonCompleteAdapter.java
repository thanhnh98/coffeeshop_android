package fpo.thanh.coffeeshop.presentation.ui.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.contanst.STATUS_ORDER;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.domain.executor.impl.ThreadExecutor;
import fpo.thanh.coffeeshop.domain.repository.Impl.SyncOrderRepositoryImpl;
import fpo.thanh.coffeeshop.presentation.presenter.SyncOrderPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.impl.SyncOrderPresenterImpl;
import fpo.thanh.coffeeshop.presentation.ui.activity.MainActivity;
import fpo.thanh.coffeeshop.presentation.ui.activity.SupportActivity;
import fpo.thanh.coffeeshop.presentation.ui.fragment.DetailSingleOrderFragment;
import fpo.thanh.coffeeshop.shareModel.PostChiTietModel;
import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;
import fpo.thanh.coffeeshop.storage.savePreference.CacheClient;
import fpo.thanh.coffeeshop.threading.MainThreadImpl;

public class ListHoaDonCompleteAdapter extends RecyclerView.Adapter<ListHoaDonCompleteAdapter.ViewHolder> {
    private final Context context;
    private final List<HoaDon> listData;
    private HoaDon hoaDon;
    private ViewBinderHelper viewBinderHelper;
    private MainActivity activity;
    private ProgressDialog progressDialog;

    public ListHoaDonCompleteAdapter(Context context, List<HoaDon> listData){
        viewBinderHelper=new ViewBinderHelper();
        viewBinderHelper.setOpenOnlyOne(true);
        this.context = context;
        activity=(MainActivity)context;
        progressDialog = new ProgressDialog(activity);

        this.listData = listData;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listhoadon_completed,parent,false));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        hoaDon=listData.get(position);
        holder.tv_maHoaDon.setText(hoaDon.getMaHoaDonLocal());
        holder.tv_giaSanPhamHoaDon.setText(String.valueOf(hoaDon.getThanhTien()));
        holder.tv_tenBanHoaDon.setText(String.valueOf(hoaDon.getTenBan()));
        holder.tv_thoiGianHoaDon.setText(hoaDon.getThoiGianLap());
        holder.tv_trangThaiHoaDon.setText(STATUS_ORDER.STATUS_ORDER(hoaDon.getTrangThai()));
        //holder.tv_trangThaiHoaDon.setTextColor(context.getResources().getColor(R.color.status_completed,null));
        viewBinderHelper.bind(holder.swipeHoaDonPending,listData.get(position).maHoaDonLocal);
        setStatusStyle(holder.tv_trangThaiHoaDon,hoaDon.getTrangThai());
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, SyncOrderPresenter.View
    {
        @BindView(R.id.tv_tenBanHoaDon)
        TextView tv_tenBanHoaDon;
        @BindView(R.id.tv_thoiGianHoaDon)
        TextView tv_thoiGianHoaDon;
        @BindView(R.id.tv_maHoaDon)
        TextView tv_maHoaDon;
        @BindView(R.id.tv_trangThaiHoaDon)
        TextView tv_trangThaiHoaDon;
        @BindView(R.id.tv_giaSanPhamHoaDon)
        TextView tv_giaSanPhamHoaDon;
        @BindView(R.id.layout_completed)
        ConstraintLayout layout_completed;

        @BindView(R.id.tv_huyHoaDon)
        TextView tv_huyHoaDon;
        @BindView(R.id.swipeHoaDonPending)
        SwipeRevealLayout swipeHoaDonPending;
        private SyncOrderPresenter presenter;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            layout_completed.setOnClickListener(this);
            tv_huyHoaDon.setOnClickListener(this);
            presenter=new SyncOrderPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),this,new SyncOrderRepositoryImpl());

        }

        @Override
        public void onClick(View v) {
            if(v.getId()==R.id.layout_completed){
                Intent intent=new Intent(context, SupportActivity.class);
                //Log.e("maHoadonLocal","ở adapter: "+listData.get(getAdapterPosition()).getMaHoaDonLocal());
                intent.putExtra("maHoaDonLocal",listData.get(getAdapterPosition()).getMaHoaDonLocal());
                intent.putExtra("requestCode",1);
                context.startActivity(intent);
            }
            else if(v.getId()==R.id.tv_huyHoaDon) {
                if (listData.get(getAdapterPosition()).getTrangThai()==3){
                    Toast.makeText(context, "Hóa đơn này đã hủy", Toast.LENGTH_SHORT).show();
                }
                else if (listData.get(getAdapterPosition()).getTrangThai()>=6){
                    Toast.makeText(context, "Không thể hủy hóa đơn này", Toast.LENGTH_SHORT).show();
                }else {
                    new AlertDialog.Builder(context).setMessage("Chắc chắn hủy hóa đơn này?")
                            .setTitle("Xác nhận hủy")
                            .setPositiveButton("Hủy đơn", (dialog, which) -> {
                                HoaDon hoaDon = listData.get(getAdapterPosition());
                                presenter.requestSyncOrder(hoaDon.getMaHoaDon(), hoaDon.getMaHoaDonLocal(), CacheClient.getCache(activity, "maNhanVien"), hoaDon.getThoiGianLap(),
                                        hoaDon.getMaBan(), hoaDon.getTenBan(), CacheClient.getCache(activity, "tokenfirebase"), hoaDon.tongTien, 0,
                                        hoaDon.thanhTien, convertJsonFromListDsMon(activity.mDB.dsMonDAO().getDsMonByMaHoadonLocal(hoaDon.getMaHoaDonLocal())), 3);
                                progressDialog.setMessage("Hủy hóa đơn...");
                                progressDialog.show();
                                // notifyItemRemoved(getAdapterPosition());
                            }).setNegativeButton("Đóng", ((dialog, which) -> dialog.dismiss())).show();
                }
            }
        }

        @Override
        public void onShowResultSyncOrder(SyncOrderModel result) {
            progressDialog.dismiss();
            if (result.getExitCode()==1){
                HoaDon hoaDon=new HoaDon();
                hoaDon = activity.mDB.hoaDonDAO().getHoaDonByMaHoaDonLocal(result.getData().getMaHoaDonLocal());
                hoaDon.setTrangThai(result.getData().getTrangThai());
                activity.mDB.hoaDonDAO().insertHoaDon(hoaDon);
                listData.get(getAdapterPosition()).setTrangThai(3);
                notifyDataSetChanged();
                Toast.makeText(context, "Hủy thành công", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(context, "Có lỗi trong quá trình hủy", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setStatusStyle(TextView tv, int s){
        switch (s){
            case 1: { //new (hóa đơn mới)
                //Log.e("??","sao ko dodoir mauf ta");
                tv.setTextColor(context.getResources().getColor(R.color.status_pending,null));
                break;
            }
            case 2: { //update (đã cập nhật)
                tv.setTextColor(context.getResources().getColor(R.color.status_capnhat,null));
                break;
            }
            case 3: { //cancel (đã hủy)
                tv.setTextColor(context.getResources().getColor(R.color.status_huy,null));
                break;
            }
            case 4: { //change ignore
                tv.setTextColor(context.getResources().getColor(R.color.status_pending,null));
                break;
            }
            case 5: { //merge
                tv.setTextColor(context.getResources().getColor(R.color.status_pending,null));
                break;
            }
            case 6: { //cooking (đang chế biến)
                tv.setTextColor(context.getResources().getColor(R.color.status_cooking,null));
                break;
            }
            case 7: { //cooked (đã trả món)
                tv.setTextColor(context.getResources().getColor(R.color.status_cooked,null));
                break;
            }
            case 8: { //complete (đã thanh toán)
                tv.setTextColor(context.getResources().getColor(R.color.status_completed,null));
                break;
            }
        }
    }
    private String convertJsonFromListDsMon(List<DsMon> dsMons){
        String json="";
        Log.e("size",dsMons.size()+"");

        json="[";
        Gson gson=new Gson();
        for(DsMon dsMon:dsMons){
            Log.e("tesst convertjson", dsMon.getMaChiTietLocal());
            json+=gson.toJson(new PostChiTietModel(0,dsMon.getMaHoaDon(),dsMon.getMaHoaDonLocal(),dsMon.getMaThucDon(),
                    dsMon.getSoLuong(),dsMon.getDonGia(),dsMon.getMaChiTietLocal(),dsMon.getTrangThai()));
            json+=",";
        }
        StringBuilder resultJson=new StringBuilder(json);
        if(resultJson.length()>1)
            resultJson.setCharAt(json.length()-1,']');
        else return json+']';
        Log.e("Json peding",resultJson.toString());
        return resultJson.toString();
    }
}
