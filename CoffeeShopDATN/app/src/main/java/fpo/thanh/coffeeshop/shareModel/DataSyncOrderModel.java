package fpo.thanh.coffeeshop.shareModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSyncOrderModel {
    @SerializedName("maHoaDon")
    @Expose
    private Integer maHoaDon;
    @SerializedName("thoiGianLap")
    @Expose
    private String thoiGianLap;
    @SerializedName("maNhanVienOrder")
    @Expose
    private Object maNhanVienOrder;
    @SerializedName("maBan")
    @Expose
    private Integer maBan;
    @SerializedName("tongTien")
    @Expose
    private Double tongTien;
    @SerializedName("maHoaDonLocal")
    @Expose
    private String maHoaDonLocal;
    @SerializedName("trangThai")
    @Expose
    private Integer trangThai;
    @SerializedName("giamGia")
    @Expose
    private Integer giamGia;
    @SerializedName("thanhTien")
    @Expose
    private Double thanhTien;
    @SerializedName("maThuNgan")
    @Expose
    private Object maThuNgan;

    public Integer getMaHoaDon() {
        return maHoaDon;
    }

    public void setMaHoaDon(Integer maHoaDon) {
        this.maHoaDon = maHoaDon;
    }

    public String getThoiGianLap() {
        return thoiGianLap;
    }

    public void setThoiGianLap(String thoiGianLap) {
        this.thoiGianLap = thoiGianLap;
    }

    public Object getMaNhanVienOrder() {
        return maNhanVienOrder;
    }

    public void setMaNhanVienOrder(Object maNhanVienOrder) {
        this.maNhanVienOrder = maNhanVienOrder;
    }

    public Integer getMaBan() {
        return maBan;
    }

    public void setMaBan(Integer maBan) {
        this.maBan = maBan;
    }

    public Double getTongTien() {
        return tongTien;
    }

    public void setTongTien(Double tongTien) {
        this.tongTien = tongTien;
    }

    public String getMaHoaDonLocal() {
        return maHoaDonLocal;
    }

    public void setMaHoaDonLocal(String maHoaDonLocal) {
        this.maHoaDonLocal = maHoaDonLocal;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public Integer getGiamGia() {
        return giamGia;
    }

    public void setGiamGia(Integer giamGia) {
        this.giamGia = giamGia;
    }

    public Double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(Double thanhTien) {
        this.thanhTien = thanhTien;
    }

    public Object getMaThuNgan() {
        return maThuNgan;
    }

    public void setMaThuNgan(Object maThuNgan) {
        this.maThuNgan = maThuNgan;
    }
}
