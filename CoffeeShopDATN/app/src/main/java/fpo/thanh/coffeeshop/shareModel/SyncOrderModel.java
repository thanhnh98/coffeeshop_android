package fpo.thanh.coffeeshop.shareModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SyncOrderModel {
    @SerializedName("exit_code")
    @Expose
    private Integer exitCode;
    @SerializedName("data")
    @Expose
    private DataSyncOrderModel data;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getExitCode() {
        return exitCode;
    }

    public void setExitCode(Integer exitCode) {
        this.exitCode = exitCode;
    }

    public DataSyncOrderModel getData() {
        return data;
    }

    public void setData(DataSyncOrderModel data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
