package fpo.thanh.coffeeshop.domain.Room.RoomDAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import fpo.thanh.coffeeshop.domain.Room.RoomModel.NotificationModel;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface NotificationDao {
    @Insert(onConflict = REPLACE)
    public void insertNotification(NotificationModel notificationModel);

    @Query("select * from NotificationModel order by _id desc")
    public List<NotificationModel> getAllNotification();
}
