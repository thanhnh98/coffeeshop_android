package fpo.thanh.coffeeshop.presentation.presenter.impl;

import fpo.thanh.coffeeshop.domain.executor.Executor;
import fpo.thanh.coffeeshop.domain.executor.MainThread;
import fpo.thanh.coffeeshop.domain.executor.impl.ThreadExecutor;
import fpo.thanh.coffeeshop.domain.interactors.PushDeviceTokenInteractor;
import fpo.thanh.coffeeshop.domain.interactors.impl.PushDeviceTokenInteractorImpl;
import fpo.thanh.coffeeshop.domain.repository.PushDeviceTokenRepository;
import fpo.thanh.coffeeshop.presentation.presenter.AbstractPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.PushDeviceTokenPresenter;
import fpo.thanh.coffeeshop.shareModel.PushTokenModel;
import fpo.thanh.coffeeshop.threading.MainThreadImpl;

public class PushDeviceTokenPresenterImpl extends AbstractPresenter implements PushDeviceTokenPresenter, PushDeviceTokenInteractor.Callback {
    private final View view;
    private final PushDeviceTokenRepository repository;

    public PushDeviceTokenPresenterImpl(Executor executor, MainThread mainThread, View view, PushDeviceTokenRepository repository) {
        super(executor, mainThread);
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void onFinish(PushTokenModel result) {
        view.showResultPushToken(result);
    }

    @Override
    public void requestPushToken(String token) {
        PushDeviceTokenInteractor interactor = new PushDeviceTokenInteractorImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this,
                token, repository);
        interactor.execute();

    }
}
